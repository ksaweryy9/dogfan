<?php
$context = Timber::get_context();
$post = Timber::query_post('ThemePost');
$context['post'] = $post;
$param = TimberUrlHelper::get_params(0); // zmienic na (1) jesli dev - odpowiada za wyswietlanie h1 author w archies
$context['param'] = $param;
$context['term_page'] = new Timber\Term();
$context['posts'] = new Timber\PostQuery();
$context['pagination'] = $context['posts']->pagination(3);
Timber::render( 'views/templates/archive.twig', $context );