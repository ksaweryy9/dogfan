<?php
function setup() {
  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain( 'nroom', get_template_directory() . '/lang' );

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support( 'title-tag' );

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support( 'post-thumbnails' );

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  //add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  // add_editor_style(Assets\asset_path('styles/main.css'));

  // Add Them Support for lqip compresion img
  add_theme_support( 'lqip' );
  // if you want to specify what image sizes to generate lqip
  // add_theme_support( 'lqip', array( 'thumbnail', 'medium' )
  
  add_theme_support( 'post-formats', array( 'gallery', 'video', 'aside' ) );
}
add_action( 'after_setup_theme', 'setup' );

/**
 * Theme assets
 */

function assets() {
  wp_enqueue_style( 'nroom/css', asset_path( 'css/app.css' ), false, null);

  if (is_single() && comments_open() && get_option( 'thread_comments' )) {
    wp_enqueue_script( 'comment-reply' );
  }

  wp_enqueue_script( 'nroom/js', asset_path( 'js/app.js' ), ['jquery'], null, true);
}
add_action( 'wp_enqueue_scripts', 'assets', 100 );

// ACF Sync Fields
add_filter( 'acf/settings/save_json', 'acf_json_save_point' );

function acf_json_save_point( $path ) {
    // update path
    $path = get_stylesheet_directory() . '/acf-fields';

    // return
    return $path;
}

add_filter( 'acf/settings/load_json', 'acf_json_load_point' );

function acf_json_load_point( $paths ) {
    // remove original path (optional)
    unset($paths[0]);

    // append path
    $paths[] = get_stylesheet_directory() . '/acf-fields';

    // return
    return $paths;
}

/**
 * Sober Intervention - cleaning wp-admin
 * more info on https://github.com/soberwp/intervention
 */
use function Sober\Intervention\intervention;

if ( function_exists( 'Sober\Intervention\intervention' ) ) {
  intervention( 'add-acf-page', 'Theme Options', [ 'administrator' ] );
  intervention( 'add-dashboard-redirect' );
  intervention( 'add-svg-support' );
}

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
  register_nav_menu('slide-menu',__( 'Slide Menu' ));
  register_nav_menu('footer-1',__( 'Footer Menu 1' ));
  register_nav_menu('footer-2',__( 'Footer Menu 2' ));
  register_nav_menu('footer-3',__( 'Footer Menu 3' ));
}
add_action( 'after_setup_theme', 'register_my_menu' );

/**
 * Register fields from class ThemPost
 */
$theme_post_class = new CustomFields(get_template_directory().'/lib/class-post.php');
$theme_post_class->register_filds();


/**
 * Disable the emoji's
 */
function disable_emojis() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
  add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
 }
 add_action( 'init', 'disable_emojis' );
 
 /**
  * Filter function used to remove the tinymce emoji plugin.
  * 
  * @param array $plugins 
  * @return array Difference betwen the two arrays
  */
 function disable_emojis_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
  return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
  return array();
  }
 }
 
 /**
  * Remove emoji CDN hostname from DNS prefetching hints.
  *
  * @param array $urls URLs to print for resource hints.
  * @param string $relation_type The relation type the URLs are printed for.
  * @return array Difference betwen the two arrays.
  */
 function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
  if ( 'dns-prefetch' == $relation_type ) {
  /** This filter is documented in wp-includes/formatting.php */
  $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );
 
 $urls = array_diff( $urls, array( $emoji_svg_url ) );
  }
 
 return $urls;
 }