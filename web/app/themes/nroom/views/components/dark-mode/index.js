import $ from 'jquery';

jQuery("#dark-mode").click(function() {
  localStorage.setItem('mode', (localStorage.getItem('mode') || 'light') === 'dark' ? 'light' : 'dark');
  localStorage.getItem('mode') === 'dark' ? document.querySelector('body').classList.add('theme--dark') : document.querySelector('body').classList.remove('theme--dark');
  localStorage.getItem('mode') === 'light' ? document.querySelector('body').classList.add('theme--default') : document.querySelector('body').classList.remove('theme--default');
})
jQuery("#dark-mode-mobile").click(function() {
  localStorage.setItem('mode', (localStorage.getItem('mode') || 'light') === 'dark' ? 'light' : 'dark'); 
  localStorage.getItem('mode') === 'dark' ? document.querySelector('body').classList.add('theme--dark') : document.querySelector('body').classList.remove('theme--dark');
  localStorage.getItem('mode') === 'light' ? document.querySelector('body').classList.add('theme--default') : document.querySelector('body').classList.remove('theme--default');
})
if (localStorage.getItem('mode') === 'light' || !localStorage.getItem('mode')) {
  jQuery('body').addClass('theme--default');
  jQuery( "#dark-mode" ).prop( "false", true );
  jQuery( "#dark-mode-mobile" ).prop( "false", true );
} else if (localStorage.getItem('mode') === 'dark') {
  jQuery('body').addClass('theme--dark');
  jQuery( "#dark-mode" ).prop( "checked", true );
  jQuery( "#dark-mode-mobile" ).prop( "checked", true );
}