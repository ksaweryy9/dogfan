<?php
add_action("acf/init", "polecane_karmy_block");
function polecane_karmy_block () {
  if( function_exists("acf_register_block") ) {
    acf_register_block(array(
      "name" => "polecane_karmy",
      "title" => __("polecane_karmy"),
      "description" => __(""),
      "render_callback" => "polecane_karmy_block_callback",
      "category"=> "formatting",
      "icon"=> "admin-comments",
      "supports" => array(
        "align" => false,
      ),
      //"keywords" => array( "testimonial", "quote" ),
    ));
  }
}

function polecane_karmy_block_callback( $block ) {
  $posts_per_pege = get_field("post_numer_in_block");
  $vars = [];
  create_posts_block($block, $posts_per_pege, $vars, __DIR__);
}

if( function_exists("acf_add_local_field_group") ):

  acf_add_local_field_group(array(
    "key" => "group_".md5("polecane_karmy"),
    "title" => "Liczba postów",
    "fields" => array(
      array(
        "key" => "field_".md5("polecane_karmy"),
        "label" => "Liczba postów w bloku",
        "name" => "post_numer_in_block",
        "type" => "number",
        "instructions" => "",
        "required" => 1, //chenge to 0 if block have constanst post_number_in_block
        "readonly" => 0,
        "conditional_logic" => 0,
        "wrapper" => array(
          "width" => "",
          "class" => "",
          "id" => "",
        ),
        "default_value" => "",  //put constanst post_number_in_block
        "placeholder" => "",
        "prepend" => "",
        "append" => "",
        "min" => "",
        "max" => "",
        "step" => "",
      ),
    ),
    "location" => array(
      array(
        array(
          "param" => "block",
          "operator" => "==",
          "value" => "acf/polecane-karmy",
        ),
      ),
    ),
    "menu_order" => 0,
    "position" => "normal",
    "style" => "default",
    "label_placement" => "top",
    "instruction_placement" => "label",
    "hide_on_screen" => "",
    "active" => true,
    "description" => "",
  ));
  
endif;