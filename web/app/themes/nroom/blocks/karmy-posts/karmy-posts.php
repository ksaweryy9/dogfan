<?php
add_action("acf/init", "karmy_posts_block");
function karmy_posts_block () {
  if( function_exists("acf_register_block") ) {
    acf_register_block(array(
      "name" => "karmy_posts",
      "title" => __("karmy_posts"),
      "description" => __(""),
      "render_callback" => "karmy_posts_block_callback",
      "category"=> "formatting",
      "icon"=> "admin-comments",
      "supports" => array(
        "align" => false,
      ),
      //"keywords" => array( "testimonial", "quote" ),
    ));
  }
}

function karmy_posts_block_callback( $block ) {
  $posts_per_pege = get_field("post_numer_in_block");
  $vars = [];
  create_posts_block($block, $posts_per_pege, $vars, __DIR__);
}

if( function_exists("acf_add_local_field_group") ):

  acf_add_local_field_group(array(
    "key" => "group_".md5("karmy_posts"),
    "title" => "Liczba postów",
    "fields" => array(
      array(
        "key" => "field_".md5("karmy_posts"),
        "label" => "Liczba postów w bloku",
        "name" => "post_numer_in_block",
        "type" => "number",
        "instructions" => "",
        "required" => 1, //chenge to 0 if block have constanst post_number_in_block
        "readonly" => 0,
        "conditional_logic" => 0,
        "wrapper" => array(
          "width" => "",
          "class" => "",
          "id" => "",
        ),
        "default_value" => "",  //put constanst post_number_in_block
        "placeholder" => "",
        "prepend" => "",
        "append" => "",
        "min" => "",
        "max" => "",
        "step" => "",
      ),
    ),
    "location" => array(
      array(
        array(
          "param" => "block",
          "operator" => "==",
          "value" => "acf/karmy-posts",
        ),
      ),
    ),
    "menu_order" => 0,
    "position" => "normal",
    "style" => "default",
    "label_placement" => "top",
    "instruction_placement" => "label",
    "hide_on_screen" => "",
    "active" => true,
    "description" => "",
  ));
  
endif;