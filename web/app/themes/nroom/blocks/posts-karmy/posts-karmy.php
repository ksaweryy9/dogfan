<?php
add_action("acf/init", "posts_karmy_block");
function posts_karmy_block () {
  if( function_exists("acf_register_block") ) {
    acf_register_block(array(
      "name" => "posts_karmy",
      "title" => __("posts_karmy"),
      "description" => __(""),
      "render_callback" => "posts_karmy_block_callback",
      "category"=> "formatting",
      "icon"=> "admin-comments",
      "supports" => array(
        "align" => false,
      ),
      //"keywords" => array( "testimonial", "quote" ),
    ));
  }
}

function posts_karmy_block_callback( $block ) {
  $context = Timber::context();
  $context['block'] = $block;
  $context['fields'] = get_fields();
  $args = [
    'post_type' => array('karmy_mokre', 'karmy_suche'), //slug cpt
    'posts_per_page' => -1,
    'orderby' => 'date',
    'post_parent' => 0
   ];
  $context['karmy'] = Timber::get_posts($args, 'ThemePost');
  Timber::render( 'PostsKarmy.twig', $context );
}