<?php
/**
 * Easy adds lqip to WordPress
 * 
 * @global array $_wp_theme_features
 * @global array $_wp_additional_image_sizes
 * @param array $metadata
 * @param int $attachment_id
 * @return array $metadata
 */
function theme_lqip_support( $metadata, $attachment_id ) {
	global $_wp_theme_features;

	if ( ! isset( $_wp_theme_features['lqip'] ) )
		return $metadata;

	global $_wp_additional_image_sizes;

	if ( is_array( $_wp_theme_features['lqip'] ) ) {
		$image_sizes = $_wp_theme_features['lqip'][0];
	} else {
    $image_sizes = get_intermediate_image_sizes();
    array_push($image_sizes, 'full');
		$image_sizes = apply_filters( 'intermediate_image_sizes_advanced', $image_sizes );
	}

	$file = get_attached_file( $attachment_id );
	$quality = apply_filters( 'lqip_quality', 10 );

	if ( file_to_lqip( $attachment_id, $file  ) ) {
    $path_parts = pathinfo( $file );
		foreach ( $image_sizes as $size ) {
      if ( isset( $metadata['sizes'][$size] ) || $size == 'full' ) {
        if ($size == 'full') {
          $width = $metadata['width'];
          $height = $metadata['height'];
          $crop = false;
          $file_path = preg_replace('/\d+\//', '', $metadata['file']);
        } else {
          $file_path = $metadata['sizes'][$size]['file'];
          if ( isset( $_wp_additional_image_sizes[$size]['width'] ) )
            $width = intval( $_wp_additional_image_sizes[$size]['width'] );
          else
            $width = get_option( "{$size}_size_w" );

          if ( isset( $_wp_additional_image_sizes[$size]['height'] ) )
            $height = intval( $_wp_additional_image_sizes[$size]['height'] );
          else
            $height = get_option( "{$size}_size_h" );

          if ( isset( $_wp_additional_image_sizes[$size]['crop'] ) )
            $crop = intval( $_wp_additional_image_sizes[$size]['crop'] );
          else
            $crop = get_option( "{$size}_crop" );
        }
        $filename = str_replace( '.' . $path_parts['extension'], '-lqip.' . $path_parts['extension'], $file_path );
        if (extension_loaded('imagick')) {
          $image = wp_get_image_editor( $file, array( 'methods' => array( 'lqip_blur' ) ) );
        } else {
          $image = wp_get_image_editor( $file );
        }
        $image->set_quality( $quality );
        $image->resize( $width, $height, $crop );
        if (extension_loaded('imagick')) {
          $image->lqip_blur();
        }
        $image->save( $path_parts['dirname'] . '/' . $filename );

        if ( ! is_wp_error( $image ) ) {
          if ($size == 'full') { 
            $metadata['lqip']['sizes'][$size]['width'] = $metadata['width'];
            $metadata['lqip']['sizes'][$size]['height'] = $metadata['height'];
            $metadata['lqip']['sizes'][$size]['mime-type'] = get_post_mime_type( $attachment_id );
          } else {
            $metadata['lqip']['sizes'][$size] = $metadata['sizes'][$size];
          }
          $metadata['lqip']['sizes'][$size]['file'] = $filename;
        }
      }
		}
	} else {
    $metadata['lqip'] = false;
  }

	return $metadata;
}
add_filter( 'wp_generate_attachment_metadata', 'theme_lqip_support', 10, 2 );
