<?php
/** 
 * Żeby lqip zadziałał musimy zmienić standardowe taki img na
 * zgodne z https://github.com/aFarkas/lazysizes
 * 
 * @param $content string wher repalce img tag
 */
function lqip_replace_img_tag( $content ) {
  global $_wp_theme_features;

	if ( ! isset( $_wp_theme_features['lqip'] ) )
		return $content;

  preg_match_all( '/<img.*?>/', $content, $img_tags );
  foreach ($img_tags[0] as $img_tag) {
    //var_dump($img_tag);
    // preg_match_all('/wp-image-\d+/', $img_tag, $clases_with_id);
    // $attachment_id = (int)preg_replace('/wp-image-/', '', $clases_with_id[0][0]);
    // $attachment_meta = wp_get_attachment_metadata($attachment_id);
    
    $lqip_img_tag = get_lqip_img_tag( $img_tag );
    $content = str_replace( $img_tag, $lqip_img_tag, $content );
  }
  return $content;
}
if (is_single()){
  add_filter( 'the_content', 'lqip_replace_img_tag' );
}