<?php
/**
 * 
 * @param string $attachment_src
 * @return string attacment source after lqip compres
 */
function get_lgip_src( $attachment_src ) {
  $file_name = pathinfo( $attachment_src )["filename"];
  $lqip_attachment_src = str_replace( $file_name, $file_name. '-lqip', $attachment_src );
  return $lqip_attachment_src;
}