<?php
/**
 * 
 * @param string $img_tag for example "<img src="" alt="" class="" etc. />"
 * @return string img tag with https://github.com/aFarkas/lazysizes
 */
function get_lqip_img_tag( $img_tag ) {
  preg_match_all('/http([^"]*)/', $img_tag, $img_src);
  $attachment_src = $img_src[0][0];
  $lqip_attachment_src = get_lgip_src( $attachment_src );
  $lqip_img_tag = preg_replace( '/src="http:(\/([a-z0-9+$_%-:]\.?)+)*\/?"/', 'src="' . $lqip_attachment_src . '"', $img_tag );
  $lqip_img_tag = preg_replace( '/class="/', 'class="lazyload blur-up ', $lqip_img_tag );
  $lqip_img_tag = str_replace( 'srcset', 'data-srcset', $lqip_img_tag );
  $lqip_img_tag = str_replace( '<img ', '<img data-src="' . $attachment_src . '" ', $lqip_img_tag );
  return $lqip_img_tag;
}