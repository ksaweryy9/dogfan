<?php
/**
 * Gdy user wrzuca pliki do media musimy sprawdzić czy jest to typ
 * pliku na którym chcemy wykonac lqip.
 * Interesują nas pliki jpg, png, bmp i chcemy wykluczyć gify bo na nich
 * są wykonywane transformacje do mp4.
 */
function file_to_lqip( $attachment_id, $file  ) {
  if (
  preg_match( '!^image/!', get_post_mime_type( $attachment_id ) ) &&
  file_is_displayable_image( $file ) && 
  !preg_match( '/gif/', get_post_mime_type( $attachment_id ) )) {
    return true;
  } else {
    return false;
  }

}