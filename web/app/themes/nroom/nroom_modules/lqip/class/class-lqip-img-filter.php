<?php
require_once(ABSPATH . WPINC . '/class-wp-image-editor.php');
require_once(ABSPATH . WPINC . '/class-wp-image-editor-imagick.php');

class Lqip_Filters_Editor extends WP_Image_Editor_Imagick {
  public function lqip_blur($radius = 10, $sigma = 100) {
    $this->image->blurImage($radius, $sigma);
    return true;
  }
}

function lqip_add_image_filters_editors( $editors ) {
  array_unshift( $editors, 'Lqip_Filters_Editor' );

  return $editors;
}
add_filter( 'wp_image_editors', 'lqip_add_image_filters_editors' );