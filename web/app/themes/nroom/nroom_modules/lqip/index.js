import lazysizes from "lazysizes";
import unveilhooks from "lazysizes/plugins/unveilhooks/ls.unveilhooks";

window.lazySizesConfig = window.lazySizesConfig || {};
window.lazySizesConfig.expand = 0;
window.lazySizesConfig.expFactor = 0;
