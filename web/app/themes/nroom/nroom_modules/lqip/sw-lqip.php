<?php
foreach (glob( __DIR__ . "/config/*.php") as $filename) {
  include $filename;
}

include __DIR__ . "/class/class-lqip-img-filter.php";

foreach (glob( __DIR__ . "/functions/*.php") as $filename) {
  include $filename;
}

function sanitize_filename( $filename ) {
  $file_parts = explode( '.', $filename );
  $extension  = array_pop( $file_parts );
  $filename   = sanitize_title( preg_replace( '/[^A-Za-z0-9\-]/', '', join( '.', $file_parts ) ) );

  return sprintf('%s.%s', $filename, $extension);
}

add_filter( 'sanitize_file_name', 'sanitize_filename' );
