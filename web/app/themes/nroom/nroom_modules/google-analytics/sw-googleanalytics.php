<?php
add_filter('acf/settings/load_json', 'sw_googleanalytics_acf_json_load_point');
function sw_googleanalytics_acf_json_load_point( $paths ) {
  $paths[] = dirname(__FILE__) . '/acf-fields';
  return $paths;
}

function ga_content_group() {
  global $post;
  if( is_single() ) {
    $categories = get_the_category( $post->ID );
    $tags = get_the_tags( $post->ID );
    if ( $categories ) {
      foreach( $categories as $category ) {
        echo "ga('set', 'contentGroup1', '$category->slug' );";
      }
    }
    if ( $tags ) {
      foreach( $tags as $tag ) {
        echo "ga('set', 'contentGroup2', '$tag->slug' );";
      }
    }
  }
}

function add_ga_script_to_head() {
  if (get_field('google_analytics_id', 'option')) {
    $ga_id = get_field('google_analytics_id', 'option');
    $ga_id_client_tracker = get_field('google_analytics_id_clientTracker', 'option');

    if ($ga_id_client_tracker) {
      $client_tracker_skript = "ga('create', '".$ga_id_client_tracker."', 'auto', 'clientTracker');";
      $client_tracker_skript_event = "ga('clientTracker.send', 'pageview');
      setTimeout(\" ga('clientTracker.send', 'event', 'read', '15 seconds')\" , 15000 );";
    } else {
      $client_tracker_skript = "";
      $client_tracker_skript_event = "";
    }

    echo "
    <!-- Google Analytics -->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', '".$ga_id."', 'auto');
      ".$client_tracker_skript;

      ga_content_group();

      echo "ga('send', 'pageview');
      setTimeout(\" ga('send', 'event', 'read', '15 seconds')\" , 15000 );
      ".

      $client_tracker_skript_event

    ."</script>
    <!-- End Google Analytics -->
    ";
  }
}
//add_action( 'wp_head', 'add_ga_script_to_head', -1 );
