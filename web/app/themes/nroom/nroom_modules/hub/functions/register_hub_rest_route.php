<?php
function register_block_rest_route($data) {
  $block_content = [];

  $hub_page_id = get_hub_page_id();
  $hub_page_blocks = parse_acf_blocks(get_post_field('post_content', $hub_page_id));

  $block_id = $data['id'];
  foreach ($hub_page_blocks as $block) {
    if ($block_id == $block['attrs']['id']) {
      if($block['attrs']['data']) {
        $block_fields = $block['attrs']['data'];
      } else {
        $block_fields = [];
      }
      /**
       * Z prawej strony big-post jest umieszczony Wall w którym musimy wyświetlić 
       * najnowsze zdarzenia (posty) docelowo ma to być zrobionoe przez websockety
       * zeby osiągnąć realtime odswiezanie. Jednak na razie po prostu wyświetlamy
       * najnowsze posty
       */ 
      if($block["blockName"] == "acf/big-posts") {
        $wall = new Wall;
        foreach ($wall->get_posts() as $post) {
          $block_content['wall']['posts'][] = api_post_structure($post);
        }
      }
    }
  }
  $post_ids = get_field($block_id, $hub_page_id); 
  $block_content['fields'] = $block_fields;
  if(is_array($post_ids)) {
    foreach($post_ids as $post_id) {
      $block_content['posts'][] = api_post_structure($post_id);
    }
  } else {
    $block_content['posts'] = [];
  }


  return $block_content;
}
add_action( 'rest_api_init', function () {
  register_rest_route( 'hub/v2', '/(?P<id>\w+)', array(
    'methods' => 'GET',
    'callback' => 'register_block_rest_route',
  ) );
} );

function is_hub_page($page_id) {
  if($page_id == get_hub_page_id()) {
    return true;
  }
}

function register_hubpage_rest_route($data) {
  if(is_hub_page($data['id'])) {
    $hub_page_id = get_hub_page_id();
    $hub_page_blocks = parse_acf_blocks(get_post_field('post_content', $hub_page_id));

    $hub_page_blocks_api_structure = [];
    foreach ($hub_page_blocks as $block) {
      $hub_page_blocks_api_structure[] = [
        'blockId' => $block['attrs']['id'],
        'blockType' => ucfirst(dashesToCamelCase(str_replace("acf/", '', $block['attrs']['name'])))
      ];
    }

    return [
      'hedaer_menu' => menu_to_api('header-menu'),
      'footer_menus' => [
        [
          'name' => get_menu_name('footer-1'),
          'item' => menu_to_api('footer-1')
        ],
        [
          'name' => get_menu_name('footer-2'),
          'item' => menu_to_api('footer-2')
        ],
        [
          'name' => get_menu_name('footer-3'),
          'item' => menu_to_api('footer-3')
        ],
      ],
      'blocks' => $hub_page_blocks_api_structure
    ];
  } else {
    return 'Page is not a nrhub page !!!';
  }
}
add_action( 'rest_api_init', function () {
  register_rest_route( 'hub/v2', '/page/(?P<id>\w+)', array(
    'methods' => 'GET',
    'callback' => 'register_hubpage_rest_route',
  ) );
} );