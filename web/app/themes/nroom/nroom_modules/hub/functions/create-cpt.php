<?php
add_action( 'init', 'create_cpt' );
function create_cpt() {
  $hub_clients = get_hub_clients();
  foreach ($hub_clients as $client) {
    register_post_type( $client->slug,
      array(
        'labels' => array(
            'name' => __( 'Wpisy '.ucfirst($client->name) ),
            'singular_name' => __( $client->name )
        ),
        'public' => true,
        'show_in_rest' => true,
        'has_archive' => false,
        'rewrite' => array('slug' => $client->slug),
        "supports" => [ "title", "editor", "thumbnail", "author" ],
      )
    );
  }
}

function register_nrhub_category() {
	$labels = [
		"name" => __( "Kategorie", "nrhub" ),
		"singular_name" => __( "Kategoria", "nrhub" ),
	];
	$args = [
		"label" => __( "Kategorie", "nrhub" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'nrhub_category', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "nrhub_category",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		];
	register_taxonomy( "nrhub_category", get_hub_clients('slug'), $args );
}
add_action( 'init', 'register_nrhub_category' );

function register_nrhub_post_tag() {
	$labels = [
		"name" => __( "Tagi", "nrhub" ),
		"singular_name" => __( "Tag", "nrhub" ),
	];
	$args = [
		"label" => __( "Tagi", "nrhub" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'nrhub_post_tag', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "nrhub_post_tag",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		];
	register_taxonomy( "nrhub_post_tag", get_hub_clients('slug'), $args );
}
add_action( 'init', 'register_nrhub_post_tag' );
