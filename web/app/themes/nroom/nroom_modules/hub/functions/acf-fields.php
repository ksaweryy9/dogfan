<?php
function auto_publishing_settings() {
  $hub_clients = get_hub_clients();
  $acf_array = [];
  foreach ($hub_clients as $client) {
    $acf_array[] = array (
      'key' => 'field_' . $client->name,
      'label' => 'Autmatycznie publikuj posty z '.$client->name,
      'name' => 'auto-publish_'.$client->name,
      'type' => 'true_false',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'message' => '',
      'default_value' => 0,
      'ui' => 1,
      'ui_on_text' => '',
      'ui_off_text' => '',
    );
  }
  return $acf_array;
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'nroomHub',
		'menu_title' 	=> 'nroomHub',
		'menu_slug' 	=> 'nroom_hub',
    'capability' 	=> 'edit_posts',
    'icon_url' => 'dashicons-arrow-right-alt',
		'redirect' 	=> false
  ));
}

if( function_exists('acf_add_local_field_group') ):
  acf_add_local_field_group(array (
    'key' => 'group_5c0fef9064002',
    'title' => 'nroomHub',
    'fields' => auto_publishing_settings(),
    'location' => array (
      array (
        array (
          'param' => 'options_page',
          'operator' => '==',
          'value' => 'nroom_hub',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
  )); 
endif;

function register_hub_block_acf_fields() {
	$home_content = get_post_field('post_content', get_hub_page_id());
	$acf_array = [];
	foreach (parse_acf_blocks($home_content) as $block) {
		$acf_array[] = array(
			'key' => 'field_'.$block["attrs"]["id"],
			'label' => $block["attrs"]["id"],
			'name' => $block["attrs"]["id"],
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
      'conditional_logic' => 0,
      'readonly' => 1,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		);
	}
	return $acf_array;
}


