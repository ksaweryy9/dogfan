import React from 'react';
import ScrollTrigger from 'react-scroll-trigger';
import Single from './Single';

export default class ScrollToRead extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      viwied: false,
      isLoaded: false,
      post: '',
      sidebar_posts: []
    };
  }

  onEnterViewport() {
    if (!this.state.viwied) {
      fetch(page_url+"/wp-json/bb/v2/post/"+this.props.postId)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              isLoaded: true,
              post: result.post,
              theme: result.theme,
              sidebar_posts: result.sidebar_posts,
            });
            window.history.replaceState('object or string', this.state.post.title, this.state.post.link);
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
    }
    if (this.state.viwied && this.state.isLoaded) {
      window.history.replaceState('object or string', this.state.post.title, this.state.post.link);
    }
  }
 
  onExitViewport() {
    this.setState({
      viwied: true
    });
  }

  render(){
    const { error, isLoaded, post, theme, viwied, sidebar_posts } = this.state;
    return(
      <ScrollTrigger onEnter={this.onEnterViewport.bind(this)} onExit={this.onExitViewport.bind(this)}>
        <div className="row">
          <div className="large-12 columns">
            <div className="czytajkolejny margin-bottom40">
              <span>Przewiń stronę, by przeczytać kolejny wpis</span>
            </div>
          </div>
          <Single error={error} isLoaded={isLoaded} post={post} theme={theme} viwied={viwied} sidebar_posts={sidebar_posts} />
        </div>
      </ScrollTrigger>
    );
  }
}
