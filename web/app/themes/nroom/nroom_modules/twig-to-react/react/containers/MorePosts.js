import $ from 'jquery';
import React from 'react';
import ScrollToRead from "./ScrollToRead";

var pastPostId = $('#post-0').data('past-post');
class MorePosts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      liked: false, 
      items: JSON.parse("[" + pastPostId + "]"),
    };
  }
  render() {
    const { items } = this.state;
    return (
      <div key="xyz">
        {items.map(item => (
          <ScrollToRead 
            key={item} 
            postId={item}
          />
        ))}
      </div>
    );
  }
}

export default MorePosts;