import React from 'react';
import ReactGA from 'react-ga';
import Article from '../../../../assets/js/react/Article';

export default class Single extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
    };
  }

  render(){
    const { error, isLoaded, post, viwied, theme, sidebar_posts } = this.props;
    const loaderSrc = page_url+"/app/themes/bizblog/dist/images/loader.mp4";
    if (error) {
      return error.message;
    } else if (!isLoaded) {
      return (
        <div className="r-is-loading">
          <video width="64" height="64" autoPlay muted>
            <source src={loaderSrc} type="video/mp4" />
          </video>
        </div>
      );
    } else {
      if (!viwied) {
        window.history.replaceState('object or string', post.post_title, '/'+post.post_name)
        ReactGA.initialize(gaID);
        ReactGA.pageview('/'+post.post_name);
      }
      return(

        <Article post={post} theme={theme} sidebar_posts={sidebar_posts} />

      );
    }
  }

}