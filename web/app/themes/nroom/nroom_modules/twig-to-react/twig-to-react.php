<?php
foreach (glob( __DIR__ . "/functions/*.php") as $filename) {
  include $filename;
}
function add_page_url_for_api() {
  if (get_field('google_analytics_id', 'option')) {
    $gaID = "var gaID = '".get_field('google_analytics_id', 'option')."';";
  }
  echo "<script>
    var page_url = '".home_url()."';".
    $gaID
  ."</script>";
}
add_action( 'wp_head', 'add_page_url_for_api' );