<?php
/**
 * @param array $post_id An array of post id
 * @return array [id, name, url]
 */
function get_previus_post_taxonomy($post_id , $taxonomy_type){
  if ($taxonomy_type == 'category'){
    $post_taxonomy = wp_get_post_categories($post_id);
  } elseif ($taxonomy_type == 'post_tag') {
    $post_taxonomy = wp_get_post_tags($post_id);
  }
  $cats = [];
  foreach($post_taxonomy as $c){
    if ($taxonomy_type == 'category'){
      $cat = get_category( $c );
    } elseif ($taxonomy_type == 'post_tag') {
      $cat = get_tag( $c );
    }
    $cats[] = [
      'id'  => $cat->term_id,
      'name' => $cat->name, 
      'link' => get_category_link($cat->term_id),
    ];
  }
  return $cats;
}
