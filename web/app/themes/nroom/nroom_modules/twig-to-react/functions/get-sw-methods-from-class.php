<?php
/**
 * zalezy nam na minimalizacji powtarzania sie w kodzie dal tego wlasne funckje uzyte w timber
 * musimy wpiac do rest api wp w tym celu pobioeramy je z classy ktorej uzywamy w motywie i templetach twig
 * 
 * @param string class name
 * @return array method name with sw_
 */
function get_sw_methods_from_class($class_name){
  $methods = get_class_methods($class_name);
  $sw_methods = [];

  foreach ($methods as $method) {
    $find_sw_methods = preg_match('/nrm_.+/', $method);
    if ($find_sw_methods >= 1) {
      $sw_methods[] = $method;
    }
  }

  return $sw_methods;
}
