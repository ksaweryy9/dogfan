<?php
function get_post_by_api($data) {
  if(is_numeric($data['id'])) {
    $post_id = $data['id'];
  } else {
    $post_id = url_to_postid( get_home_url().'/'.$data['id'] );
  }
  
  $post = get_post($post_id);
  $twit = htmlspecialchars( urlencode( html_entity_decode( $post->post_title, ENT_COMPAT, 'UTF-8' ) ), ENT_COMPAT, 'UTF-8' );

  $post_categorys = get_the_category($post->ID);
  $post_categorys_array = [];
  foreach ($post_categorys as $post_category ) {
    $post_categorys_array[] = $post_category->term_id;
  }

  $args_nie_przegap = array(
    'date_query' => array( 'before' => get_the_date( 'Y-m-d H:i:s', $post->ID ) ),
    'posts_per_page' => 3,
    'category__in' => $post_categorys_array
  );

  $post_object = api_post_structure($post->ID);

  if (empty($post)) {
    return null;
  }
  return $post_object;
}
add_action( 'rest_api_init', function () {
  register_rest_route( 'bb/v2', '/post/(?P<id>\S+)', array(
    'methods' => 'GET',
    'callback' => 'get_post_by_api',
  ) );
} );
