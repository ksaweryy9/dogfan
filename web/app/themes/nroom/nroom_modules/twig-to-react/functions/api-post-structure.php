<?php
function menu_to_api($location_slug) {
  if (isset(get_nav_menu_locations()[$location_slug])) {
    $menu = wp_get_nav_menu_items(get_term(get_nav_menu_locations()[$location_slug], 'nav_menu')->name);
    $menu_api_structure =[];
    foreach ($menu as $menu_item) {
      $menu_api_structure[] = [
        'title' => $menu_item->title,
        'link' => $menu_item->url
      ];
    }
  } else {
    $menu_api_structure = [];
  }
  return $menu_api_structure;
}

function get_menu_name($location_slug) {
  if (isset(get_nav_menu_locations()[$location_slug])) {
    $locations = get_nav_menu_locations(); //get all menu locations
    $menu2 = wp_get_nav_menu_object($locations[$location_slug]);//get the menu object
    return $menu2->name;
  }
}

function editor_type($content) {
  if (strpos( $content, '<!-- wp:' ) !== false) {
    return "gutenebrg";
  } else {
    return "classic";
  }
}

function api_content($content) {
  if (editor_type($content) == "gutenebrg") {
    $content = parse_blocks($content);
  } else {
    $content = apply_filters( 'the_content', $content );
  }
  return $content;
} 

function api_post_structure($post_id) {
  $post = new ThemePost($post_id);
  $seo = new Seo($post->ID);
  $post_object = [
    'post' => [
      'editor_type' => editor_type($post->post_content),
      'format' => get_post_format($post->ID),
      'seo' => [
        'title' => $seo->title(),
        'meta' => $seo->metas(),
        'tags' => $seo->get_post_tags()
      ],
      'ID'             => $post->ID,
      'link'           => rtrim(get_permalink($post->ID), '/'),
      'relative_link'  => str_replace(get_home_url(), '', rtrim(get_permalink($post->ID), '/')),
      'name'           => $post->post_name,
      'title'          => $post->post_title,
      'content'        => api_content($post->post_content), 
      'categories'     => get_previus_post_taxonomy($post->ID, 'category'),
      'tags'           => get_previus_post_taxonomy($post->ID, 'post_tag'),
      'loading_posts'  => get_previuos_posts_id($post->ID)
    ],
    'theme' => [
      'link' => get_template_directory_uri()
    ],
    'hedaer_menu' => menu_to_api('header-menu'),
    'footer_menus' => [
      [
        'name' => get_menu_name('footer-1'),
        'item' => menu_to_api('footer-1')
      ],
      [
        'name' => get_menu_name('footer-2'),
        'item' => menu_to_api('footer-2')
      ],
      [
        'name' => get_menu_name('footer-3'),
        'item' => menu_to_api('footer-3')
      ],
    ]
  ];

  $sw_methods = get_sw_methods_from_class('ThemePost');
  foreach($sw_methods as $sw_method) {
    $post_object['post'][$sw_method] = $post->$sw_method;
  }

  return $post_object;
}
