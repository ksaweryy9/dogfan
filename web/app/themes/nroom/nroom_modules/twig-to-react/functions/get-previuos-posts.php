<?php
/**
 * @param int $post_id
 * @return string 5 post id seperate by coma
 */

function get_previuos_posts_id($post_id) { 
  $args = array(
    'date_query' => array( 'before' => get_the_date( 'Y-m-d H:i:s', $post_id ) ),
    'posts_per_page' => 5,
  );
  $posts = get_posts($args);
  $posts_id = [];

  foreach($posts as $post){
    $posts_id[] = $post->ID;  
  } 

  if ( empty( $posts ) ) {
    return null;
  }
  return  $posts_id;
}
