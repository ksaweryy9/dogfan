# Twig to React

W celu usprawnienia pracy pliki templetów twig można przy pomocy Gulpa przekonwertować na pliki js dla React JS.

# Podstawowa semantyka TWIG

Czyli jak powinno pisać się pliki twig by można je było automaktycznie przekonwertować.

---

- Nazwa zmiennej twig w nawiasach klamrowych powinna być poprzedzona i zakończona spacją.
np. `{{ nazwa }}` a nie `{{nazwa}}`
- W zmienny twig powinniśmy mieć tylko składnie obiektową, bez używania funkcji itp.
np. `{{ thumbnail.src }}` a nie `{{ thumbnail.src() }}`
- Style inline powinny być pisane zgodnie z dobrymi praktykami CSS czyli para właściwość:  wartość (nawet pojedyncza) powinna być zawsze zakończony ";".  Dodatkowo między właściwością a wartością za dwukropkiem powinna być spacja.
np.`style="color: red;"` a nie `style="color:red;" lub style="color: red"`

# Konfiguracja WordPress'a

W pliku single.php (single.twig) dodajemy:
- "<div id="previousPost"></div>" - w tym miejscu będą doaładowywane kolejne posty
- "<div id="firstPost" style="height:1px"></div>" w tym miejscu będzie przełączal się adres na pierwszy post on inview
- do znaczinka w którym trzymany jest content artykułu np <article> dodajemy id="#post-0", data-id="$curent_post_id", data-past-post="<?php get_previuos_posts_id($curent_post_id) ?>",
- w pliku single.js dodajemy "import loadNextPosts from "twig-to-react";" oraz loadNextPosts()

# Gulp 

wpisanie komendy $ gulp create-react-components tworzy w folderze react/domponents komponenty React js z volderu theme/views/components