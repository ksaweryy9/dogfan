import $ from 'jquery';
import Inview from 'jquery-inview';
import React from 'react';
import ReactDOM from 'react-dom';
import MorePosts from "./react/containers/MorePosts";

export default function loadNextPosts() {
  var pageUrl = window.location.pathname;
  $('#firstPost').on('inview', function(event, isInView) {
    if (isInView) {
      window.history.replaceState('object or string', '', pageUrl)
    } 
  });

  const domContainer = document.querySelector('#previousPost');
  ReactDOM.render(React.createElement(MorePosts), domContainer);
} 