'use strict';

import plugins       from 'gulp-load-plugins';
import gulp          from 'gulp';

var replace = require('gulp-replace');
var rename = require('gulp-rename');
var insert = require('gulp-insert');
const tap = require('gulp-tap')
const fs = require('fs');
const path = require('path');
const componentsProps = {};
const componentsInComponent = {};

function seperate_statement(array) {
  const grouped = [];
  let group = [];
  array.forEach(statement => {
    group.push(statement);
    if (statement === "{% endif %}") {
      grouped.push([...group]);
      group = [];
    }
  });
  return grouped;
}

/**
 * Create object with props for all components
 */
function createReactProps() {
  return gulp.src(['../../views/components/**/*.twig'])
    .pipe(tap(function (file, t) {
      const componentName = path.basename(file.path).replace(/\.twig/g, '');
      const props = file.contents.toString().match(/{{.+?}}|{%\s(if|elseif).+%}|{%\sfor\s.+\sin\s.+\s%}/g);
      const includeComponents = file.contents.toString().match(/{%\sinclude\s'.+\.twig'\s%}/g);
      const includeComponetsProps =[];
      if (includeComponents) {
        includeComponents.forEach(function(includeComponent){
          const filePath = '../../'+includeComponent.replace(/{%\sinclude\s'|'\s%}/g, '');
          const includeComponentProps = fs.readFileSync(filePath, "utf8").toString().match(/{{.+?}}|{%\s(if|elseif).+%}|{%\sfor\s.+\sin\s.+\s%}/g);
          includeComponetsProps.push(includeComponentProps)
        });
        const arr1d = [].concat(...includeComponetsProps)
        var allProps = props.concat(arr1d);
      } else {
        var allProps = props;
      }

      if (allProps) {
        const propsNames = [];
        allProps.forEach(function(prop){
          const findPropName = prop.match(/{{(\s\w+\.|\s\w+\s)|{%\s(if|elseif).+%}|{{\s\w+\[.+\]\s}}|{%\sfor\s.+\sin\s.+\s%}/g);
          if (findPropName) { 
            // twig conditional logic to props
            if (findPropName[0].search(/{%\s(if|elseif)\s.+%}/g)>=0) {
              if (findPropName[0].search(/\sor\s|\sand\s/g)>=0) { // operator logiczny or lub and
                findPropName[0].match(/{%\s(if|elseif)\s\S+\s|\s(or|and)\s\S+/g).forEach(function(props){
                  propsNames.push(props.replace(/{%\sif\s|\s(or|and)\s/g, '').replace(/\s/g, ''));
                })
              } else { 
                propsNames.push(findPropName[0].replace(/{%\s(if|elseif)\s|\s(or|and)\s/g, '').replace(/(\..+|\s==.+)%}|\s(%}|\W%})/g, ''));
              }
            // twig foor loop to props
            } else if (findPropName[0].search(/{%\sfor\s.+\sin\s.+\s%}/g)>=0) {
              propsNames.push(findPropName[0].replace(/{%\sfor\s.+\sin\s|\s%}|\|.+\s%}|\..+\s%}/g, ''));
            // twig loop.index to index for js map
            } else if (findPropName[0].search(/\[loop\.index\]/g)>=0) {
              propsNames.push(findPropName[0].replace(/{{\s/g, '').replace(/\s}}/g, '').replace(/\[loop\.index\]/g, ''));
            // twig variable to props
            } else { 
              propsNames.push(findPropName[0].replace(/{{\s/g, '').replace(/\./g, ''));
            } 
          } else {
            // props w twig w zlym formacie
            console.log('\x1b[33m%s\x1b[0m', file.path+' wrong foramat: '+prop);  
          }
        });
        const uniquePropNames = Array.from(new Set(propsNames));
        Object.assign(componentsProps, {[componentName]:uniquePropNames});
      } else {
        // brak props w sprawdzanym pliku
      }
    }))
}

/**
 * 
 */
function createUsedComponentsInComponent() {
  return gulp.src(['../../views/components/**/*.twig'])
  .pipe(tap(function (file, t) {
    const components = file.contents.toString().match(/{%\sinclude\s'.+\.twig'\s%}/g);
    const componentName = path.basename(file.path).replace(/\.twig/g, '');
    if (components) {
      const componentsPath = [];
      components.forEach(function(component){
        componentsPath.push(component.replace(/{%\sinclude\s'views\/components\/|'\s%}/g, ''));
      });
      const uniqueComponentsPath = Array.from(new Set(componentsPath));
      Object.assign(componentsInComponent, {[componentName]:uniqueComponentsPath});
    }
  }))
}

gulp.task('create-react-components', gulp.series(createReactProps, createUsedComponentsInComponent, function() {
  return gulp.src(['../../views/components/**/*.twig'])
    /**
     * Remove html and twig inline coments
     */
    .pipe(replace(/<!--.+?-->|{#.+?#}/g, ''))
    /**
     * Convert inline style to 
     * JavaScript object with camelCased  
     */
    .pipe(replace('class', 'className'))
    .pipe(replace(/style=".+;"/g, function( match ) {
      const styleInCamelCased = []; 
      match
        .replace(/style="/g, '')
        .split(';')
        .filter(match => match.split(/:\w|:\s/g)[0] && match.split(/:\w|:\s/g)[1])
        .map(match => [
            match.split(/:\w|:\s/g)[0].trim().replace(/-./g, c => c.substr(1).toUpperCase()),
            match.split(/:\w|:\s/g)[1].trim()
        ])
        .forEach(function(stylePropAndValue){
          if (stylePropAndValue[1].search(/.{{|}}./g) >=0 ) {
            styleInCamelCased.push(stylePropAndValue[0]+':`'+stylePropAndValue[1].replace(/{{/g, '${').replace(/}}/g, '}')+'`');
          } else if (stylePropAndValue[1].search(/{{|}}/g) >=0) {
            styleInCamelCased.push(stylePropAndValue[0]+':'+stylePropAndValue[1].replace(/{{|}}/g, ''));
          } else {
            styleInCamelCased.push(stylePropAndValue[0]+':"'+stylePropAndValue[1]+'"');
          }
        })
      return 'style={{ '+styleInCamelCased+' }}';
    }))
    /**
     * Convert twig variable to react props
     * 
     * W kodzie mamy 3 typy zmiennych np:
     * 1 src="{{variable1}}"
     * 2 <div>{{variable2}}</div>
     * 3 src="{{variable3}}/x/y.png"
     * nelzey je zmienic na skladnie zgodną z  
     */
    .pipe(replace(/(("{{|{{)\s(\w+|\.)+\s(}}|}}")((\w+|\/+|\.)+"|"|)|{{\s\w+\[.+\]\s}})/g, function(match) {  
      if (match.search(/^"{{\s(\w+|\.)+\s}}"$/gm)>=0) {
        return match.replace(/"{{\s/g, '{ ').replace(/\s}}"/g, ' }');
      } else if (match.search(/^{{\s(\w+|\.)+\s}}$/gm)>=0) {
        return match.replace(/{{\s/g, '{ ').replace(/\s}}/g, ' }');
      } else if (match.search(/^"{{\s(\w+|\.)+\s}}.+"/gm)>=0) {
        return match.replace(/"{{\s/g, '{`${ ').replace(/\s}}/g, ' }').replace(/"/g, '`}') ;
      } else if (match.search(/\[loop\.index\]/g)>=0) {
        return match.replace(/{{\s/g, '{ ').replace(/\s}}/g, ' }').replace(/\[loop\.index\]/g, '[index]');
      }
    }))
    /**
     * Convert twig for loop to array.map
     */
    .pipe(replace(/{%\sfor\s.*?\sin\s.*?\s%}/g, function(match) {
      const item = match.replace(/{%\sfor\s/g, '').replace(/\sin\s.*?\s%}/g,'');
      const items = match.replace(/{%\sfor\s[a-zA-Z]+\sin\s/g, '').replace(/\s%}|\|slice\(\d,\d\)\s%}|\|slice\(\d\)\s%}/g, '');
      // with slice
      if (match.search('slice')>0) {
        const slice = match.match(/slice\(\d\)|slice\(\d,\d\)/g);
        return '{'+items+'.'+slice[0]+'.map(('+item+', index ) => ('; 
      // without slice
      } else {
        return '{'+items+'.map(('+item+', index ) => (';
      }
    }))
    .pipe(replace('{% endfor %}', '))}'))
    /**
     * Convert twig include to react component
     */
    .pipe(replace(/{%\sinclude\s'.+\.twig'\s%}/g, function(match) {
      const componentName = match.match(/([A-Za-z0-9_-]+)\.twig/g)[0].replace(/\.twig/g, '');
      const props = [];
      componentsProps[componentName].forEach(function(prop){
        props.push(prop+'={'+prop+'}')
      });
      return '<'+componentName.replace(/-/g, '')+' '+props.join(' ')+'/>';
    }))
    /**
     * Convert twig conditional logic to babel-plugin-jsx-control-statements
     */
    .pipe(replace(/{%\s(if|elseif|else).+%}|{%\sendif\s%}/g, function(match, p1, offset, string) {
      const conditionArray= string.match(/{%\s(if|elseif|else).+%}|{%\sendif\s%}/g);
      if (conditionArray.includes('{% else %}')) {  
        if (match.search(/{%\s(if)/g)>=0) {
          return match.replace(/{%\s(if)/g, '<Choose><When condition={').replace(/\s%}/g, ' }>').replace(/\sor\s/g, ' || ').replace(/\sand\s/g, ' && ');
        } else if (match.search(/{%\s(elseif)/g)>=0) {
          return match.replace(/{%\s(elseif)/g, '</When><When condition={').replace(/\s%}/g, ' }>').replace(/\sor\s/g, ' || ').replace(/\sand\s/g, ' && ');
        } else if (match.search(/{%\selse/g)>=0) {
          return match.replace(/{%\selse/g, '</When><Otherwise').replace(/\s%}/g, '>');
        } else if (match.search(/{%\sendif\s%}/g)>=0) {
          return match.replace(/{%\sendif\s%}/g, '</Otherwise></Choose>');
        }
      } else {
        if (match.search(/{%\s(if)/g)>=0) {
          return match.replace(/{%\s(if)/g, '<Choose><When condition={').replace(/\s%}/g, ' }>').replace(/\sor\s/g, ' || ').replace(/\sand\s/g, ' && ');
        } else if (match.search(/{%\s(elseif)/g)>=0) {
          return match.replace(/{%\s(elseif)/g, '</When><When condition={').replace(/\s%}/g, ' }>').replace(/\sor\s/g, ' || ').replace(/\sand\s/g, ' && ');
        } else if (match.search(/{%\sendif\s%}/g)>=0) {
          return match.replace(/{%\sendif\s%}/g, '</When></Choose>');
        }
      }
    }))
    .pipe(replace('{% endif %}', '</When></Choose>'))
    /**
     * Add "header" and "footer" react component
     */
    .pipe(insert.transform(function(contents, file) {
      /**
       * React nie przyjmuje w props znacznikow html z tego powodu
       * musimy przekazany conent posta zmienic na dangerouslySetInnerHTML
       */
      if (contents.search(/{\spost.content\s}/g)>=0) {
        var createContent = 'function createContent() { return {__html: post.content}; }'
      } else {
        var createContent = '';
      }
      const componentName = path.basename(file.path).replace(/\.twig/g, '');
      /**
       * W plikach uzywane sa props wygnerowane z twig w formacie { nazwa }, { nazwa.inne } etc.
       * tworzymy z nich array by stworzyc reducer w pliku .js const { nazwa, nazwa.inna } = props
       */
      if (componentsProps[componentName]) {
        var propsReducer = componentsProps[componentName];
      } else {
        var propsReducer = [];
      }
      if (componentsInComponent[componentName]) {
        const allUsedComponentProps = [];
        const allComponentImport = [];
        const usedComponents = componentsInComponent[componentName];
        usedComponents.forEach(function(usedComponent){
          const usedComponentName = usedComponent.replace(/\w+\/|.twig/g, '');
          /*
           * Po za zwykłymi props uzwyanymi przez komponent mamy jeszcze props przekazywane
           * do komponetów uzywanych w komponencie dla tego tworzymy array z propsami tych komeponetnów
           * by następnie dodąć je do propsReducer i stworzyć pełnego reducera.
           */
          if (componentsProps[usedComponentName]) {
            const usedComponentProps = componentsProps[usedComponentName];
            usedComponentProps.forEach(function(usedComponentProp){
              allUsedComponentProps.push(usedComponentProp);
            });
          }
          /**
          * W plikach uzywamy jest import wygenerowany z twig zeby zadziałał
          * musimy dla kazdego uzywanego komponentu dodac 'import from "xxx"' na początku komponentu.
          */
          const usedComponentPath = usedComponent.replace(/\.twig/g, '');
          const componentPath = file.path.replace(/.+\/views\/components\//g, '').replace(/\.twig/g, '');
          if (componentPath.replace(/\w+\//g, '../').search(/\.\.\//g)>=0) {
            const importComponentLine = 'import ' + usedComponentName.replace(/-/g, '') + ' from "' + componentPath.replace(/\w+\//g, '../').replace(/\/\w+/g, '/'+usedComponentPath) + '"';
            allComponentImport.push(importComponentLine);
          } else {
            const importComponentLine = 'import ' + usedComponentName.replace(/-/g, '') + ' from "./' + usedComponentPath + '"';
            allComponentImport.push(importComponentLine);
          }
        });
        var uniqueAllComponentImport = Array.from(new Set(allComponentImport));
        var propsReducerUsedComponents = Array.from(new Set(allUsedComponentProps));
      } else {
        var propsReducerUsedComponents = [];
        var uniqueAllComponentImport = [];
      }
      /**
       * Component react wymaga odpowiedniej struktury kodu dla tego tworzymy 
       * kod który musi się znaleźć przed i za kodem samego tempaltu twig.
       * (import React from "react" etc.)
       */
      if (componentsProps[componentName] || componentsInComponent[componentName]) {
        const propsReducerAll = propsReducer.concat(propsReducerUsedComponents)
        const uniquePropsReducerAll = Array.from(new Set(propsReducerAll));
        const beforeContents = 'import React from "react";\n'+uniqueAllComponentImport.join(';\n')+'\n\nconst '+componentName.replace(/-/g, '')+' = (props) => {\n\nconst { '+uniquePropsReducerAll+' } = props;\n\n'+createContent+'\n\nreturn(\n\n';
        const afterContents = '\n)\n\n}\n\nexport default '+componentName.replace(/-/g, '')+';';
        return beforeContents+contents.replace(/{\spost.content\s}/g, '<div dangerouslySetInnerHTML={createContent()} />')+afterContents;
      } else {
        const beforeContents = 'import React from "react";\n\nconst '+componentName.replace(/-/g, '')+' = (props) => {\n\nreturn(\n\n';
        const afterContents = '\n)\n\n}\n\nexport default '+componentName.replace(/-/g, '')+';';
        return beforeContents+'\t'+contents.replace(/{\spost.content\s}/g, '<div dangerouslySetInnerHTML={createContent()} />')+afterContents;
      }
    }))
    .pipe(rename(function (path) {
      path.extname = ".js";
    }))
    .pipe(gulp.dest('../../assets/js/react/components'));
}));