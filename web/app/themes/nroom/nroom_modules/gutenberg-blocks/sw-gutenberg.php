<?php
/** 
 * Inkludowanie bloków z pojedynczych plików by zachować większą 
 * przejrzystść ich logiki działania
 */


/** 
 * Dodanie styli w celu ostylowania bloków w wp-admin  
 */
add_action( 'enqueue_block_assets', 'my_acf_block_editor_style' );
function my_acf_block_editor_style() {
  if (is_admin()) {
    wp_enqueue_style(
      'example_block_css',
      get_template_directory_uri() . '/dist/css/gutenberg-blocks.css'
    );
  }
}

/**
 * Admin moze wybrac zrodlo postów wyswietlanych w danym bloku
 */
require __DIR__ . '/class/Class-BlockPosts.php';

/**
 * By móc uzyskać większą modularność kodu całą logikę pojedynczego 
 * bloku przechowujemy w jednym folderze. Z tego powodu musimy
 * zaincludowa wszystkie pliki bloków,
 */
foreach ( glob( __DIR__ . "/blocks/*/*.php") as $filename ) {
  include $filename;
}

/**
 * oraz stworzyć foldery do zapisywania i odczytawania pól ACF
 * uzywanaych w pojedynczych blokach. 
 * 
 * By admin miał mozliwosc wyboru w ktorym bloku zapisac pola potrzebna 
 * jest wtyczka: https://github.com/USStateDept/ACF-Local-JSON-Manager
 */
add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {

  foreach ( glob( __DIR__ . "/blocks/*") as $block_dirname ) {

    if ( !file_exists($block_dirname . '/acf-fields' ) ) {
      mkdir( $block_dirname . '/acf-fields', 0774 );
    } 

    $paths[] = $block_dirname . '/acf-fields';
  }

  /**
   * Niezawsze pola ACF chcemy mieć osobno dla kazdego bloka
   * dla tego w katalogu głównym tworzeym dodatkową lokalizację
   * "główną" dla pol uzywanych globalnie w całym sw-gutenebrg
   */
  $paths[] = dirname(__FILE__) . '/acf-fields';

  return $paths;
}



/**
 * Praca z Gutnebergiem odbywa się w js a nie jak do tej pory w php
 * z tego powodu dodajemy plik js który umozliwi nam z nim prace
 */
function swgutneberg_script_enqueue() {
  wp_enqueue_script(
    'swgutneberg-script',
    get_stylesheet_directory_uri() . '/nroom_modules/gutenberg-blocks/sw-gutenebrg.js',
    array( 'wp-blocks' )
  );
}
add_action( 'enqueue_block_editor_assets', 'swgutneberg_script_enqueue' );

foreach (glob( __DIR__ . "/functions/*.php") as $filename) {
  include $filename;
}

/**
 * Plugins
 */
include __DIR__ . '/plugins/archive-maping/archive-maping.php';

