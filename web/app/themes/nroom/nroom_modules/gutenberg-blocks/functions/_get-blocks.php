<?php
function get_post_blocks() {
  $blocks = array_slice(scandir(BLOCKS_DIRECOTRY), 2);
  $acf_block_names = [];
  foreach ($blocks as $block) {
    $file_in_block_dir = scandir(BLOCKS_DIRECOTRY.$block);
    if (array_search($block.'.php' ,$file_in_block_dir)) {
      $acf_block_names[] = 'acf/'.$block;
    }
  }
  return $acf_block_names;
}

function get_no_post_blocks() {
  $blocks = array_slice(scandir(BLOCKS_DIRECOTRY), 2);
  $acf_block_names = [];
  foreach ($blocks as $block) {
    $file_in_block_dir = scandir(BLOCKS_DIRECOTRY.$block);
    if (!array_search($block.'.php' ,$file_in_block_dir)) {
      $acf_block_names[] = 'acf/'.$block;
    }
  }
  return $acf_block_names;
}