<?php
/**
 * Register Gutenberg Blocks
 */
add_action('acf/init', 'block_register');
function block_register() {
  $blocks = array_slice(scandir(BLOCKS_DIRECOTRY), 2);
  foreach ($blocks as $block) {
    $file_in_block_dir = scandir(BLOCKS_DIRECOTRY.$block);
    if (!array_search($block.'.php' ,$file_in_block_dir)) {
      $block_name = $block;
      if( function_exists('acf_register_block_type') ) {
        acf_register_block_type(array(
          'name' => strtolower($block_name),
          'title' => __($block_name),
          'description' => __('A custom testimonial block.'),
          'render_callback' => 'default_block_callback',
          'category'=> 'formatting',
          'icon'=> 'admin-comments',
          'supports' => array(
            'align' => false,
          ),
          //'keywords' => array( 'testimonial', 'quote' ),
        ));
      }
    }
  }
}

function default_block_callback($block) {
  $context = Timber::context();
  $context['block'] = $block;
  $context['fields'] = get_fields();
  if( !empty($block['className']) ) {
    $className = $block['className'];
  } else {
    $className = '';
  }
  $context['class'] = $className;
  Timber::render( BLOCKS_DIRECOTRY . strtolower($block["title"]) . '/' .ucfirst(dashesToCamelCase($block["title"])).'.twig', $context );
}

/**
 * Register ACF json load point for all block
 */
add_filter('acf/settings/load_json', function ( $paths ) {
  $blocks = array_slice(scandir(BLOCKS_DIRECOTRY), 2);
  foreach ($blocks as $block) {
    $block_name = $block;
    $paths[] = BLOCKS_DIRECOTRY . strtolower($block_name) .'/acf-fields';
  }
  return $paths;
});
