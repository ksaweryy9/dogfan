<?php
function post_type_from_hub() {
  $post_types = [
    'post' => 'Wpisy'
  ];
  if(function_exists('get_hub_clients')) {
    foreach (get_hub_clients() as $client ) {
      $post_types[$client->slug] = $client->name;
    }
  }
  return $post_types;
}

function block_location() {
  $acf_location = [];
  foreach(get_post_blocks() as $post_block) {
    $acf_location[] = array(
      array(
        'param' => 'block',
        'operator' => '==',
        'value' => $post_block,
      ),
    ); 
  }
  return $acf_location;
}


if( function_exists('acf_add_local_field_group') ):
  acf_add_local_field_group(array(
    'key' => 'group_5c9a2a89866a3',
    'title' => 'Źródło postów',
    'fields' => array(
      array(
        'key' => 'field_5c9a2aa8e932b',
        'label' => 'Źródło postów',
        'name' => 'typ_postow',
        'type' => 'radio',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'choices' => post_type_from_hub(),
        'allow_null' => 0,
        'other_choice' => 0,
        'default_value' => '',
        'layout' => 'vertical',
        'return_format' => 'value',
        'save_other_choice' => 0,
      ),
      array(
        'key' => 'field_5c9a2aa8e932a',
        'label' => '',
        'name' => 'zrodlo_postow',
        'type' => 'radio',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'choices' => array(
          'category' => 'Kategorie',
          'post_tag' => 'Tagi',
          'exact_posts' => 'Konkretne posty',
          'all' => 'Wszystkie',
          'queried_object' => 'strona typu archive (OPCJA DZIAŁA TYLKO W Archive Templates)',
        ),
        'allow_null' => 0,
        'other_choice' => 0,
        'default_value' => '',
        'layout' => 'vertical',
        'return_format' => 'value',
        'save_other_choice' => 0,
      ),
      array(
        'key' => 'field_5c9a2ad0e932c',
        'label' => 'Kategoria sekcji',
        'name' => 'category_id',
        'type' => 'taxonomy',
        'instructions' => '',
        'required' => 1,
        'conditional_logic' => array(
          array(
            array(
              'field' => 'field_5c9a2aa8e932a',
              'operator' => '==',
              'value' => 'category',
            ),
          ),
        ),
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'taxonomy' => 'category',
        'field_type' => 'select',
        'allow_null' => 0,
        'add_term' => 1,
        'save_terms' => 0,
        'load_terms' => 0,
        'return_format' => 'id',
        'multiple' => 0,
      ),
      array(
        'key' => 'field_5c9a2b4b88999',
        'label' => 'Tag sekcji',
        'name' => 'post_tag_id',
        'type' => 'taxonomy',
        'instructions' => '',
        'required' => 1,
        'conditional_logic' => array(
          array(
            array(
              'field' => 'field_5c9a2aa8e932a',
              'operator' => '==',
              'value' => 'post_tag',
            ),
          ),
        ),
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'taxonomy' => 'post_tag',
        'field_type' => 'select',
        'allow_null' => 0,
        'add_term' => 1,
        'save_terms' => 0,
        'load_terms' => 0,
        'return_format' => 'id',
        'multiple' => 0,
      ),
      array(
        'key' => 'field_5c9a2b8d8899a',
        'label' => 'Posty sekcji',
        'name' => 'posty_sekcji',
        'type' => 'repeater',
        'instructions' => '',
        'required' => 1,
        'conditional_logic' => array(
          array(
            array(
              'field' => 'field_5c9a2aa8e932a',
              'operator' => '==',
              'value' => 'exact_posts',
            ),
          ),
        ),
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'collapsed' => '',
        'min' => 0,
        'max' => 0,
        'layout' => 'row',
        'button_label' => '',
        'sub_fields' => array(
          array(
            'key' => 'field_5c9a2bb88899b',
            'label' => 'post',
            'name' => 'post',
            'type' => 'post_object',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => '',
              'id' => '',
            ),
            'post_type' => array(
              0 => 'post',
            ),
            'taxonomy' => '',
            'allow_null' => 0,
            'multiple' => 0,
            'return_format' => 'id',
            'ui' => 1,
          ),
        ),
      ),
    ),
    'location' => block_location(),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
    'modified' => 1565344491,
  ));
endif;