<?php
function get_post_id_from_timber($timber_posts) {
  $posts_id = [];
  foreach($timber_posts as $post) {
    $posts_id[] = $post->ID;
  }
  return $posts_id;
}

function create_posts_block($block, $posts_per_pege, $vars, $location) {
  global $post;
  $vars = $vars;
  $sum_of_post_on_page = get_sum_of_post_in_blocks(get_post(get_gutneberg_archive_template_id())->post_content);
  $posts = new BlockPosts($block["id"], $posts_per_pege, $sum_of_post_on_page);
  $offset = $posts->get_offset($block);
  $post_source = $posts->get_posts_from_source($offset);
  $special_posts = $post_source['wp_query_args'];
  $vars = $post_source['vars'];
  $vars['block'] = $block;
  $vars["data"]["number_of_posts_in_block"] = $posts_per_pege;
  $vars['fields'] = get_fields();
  $vars['posts'] = Timber::get_posts($special_posts, 'ThemePost');
  update_field($block["id"], get_post_id_from_timber($vars['posts']), $post->ID);
  if(is_admin()) {
    Timber::render( '/views/components/blocks/PlaceholderBlock.twig', $vars );
  } else {
    if (!empty($vars['posts'])) {
      Timber::render( $location.'/'.implode(preg_grep('/.twig/i',scandir($location))), $vars );
    }  
  }
}