<?php
function get_sum_of_post_in_blocks($conent) {
  $blocks = parse_acf_blocks($conent);
  $sum_post_in_blocks = [];
  foreach ($blocks as $block) {  
    if(isset($block["attrs"]["data"]["post_numer_in_block"])) {
      $sum_post_in_blocks[] = $block["attrs"]["data"]["post_numer_in_block"];
    } else {
      $sum_post_in_blocks[] = 0;
    }
  }
  return array_sum($sum_post_in_blocks);
}
