const disabledBlocks = [
  'core-embed/wordpress-tv',
  'core-embed/videopress',
  'core-embed/tumblr',
  'core-embed/ted',
  'core-embed/speaker',
  'core-embed/smugmug',
  'core-embed/slideshare',
  'core-embed/scribd',
  'core-embed/screencast',
  'core-embed/reverbnation',
  'core-embed/reddit',
  'core-embed/polldaddy',
  'core-embed/photobucket',
  'core-embed/mixcloud',
  'core-embed/meetup-com',
  'core-embed/kickstarter',
  'core-embed/issuu',
  'core-embed/imgur',
  'core-embed/hulu',
  'core-embed/funnyordie',
  'core-embed/dailymotion',
  'core-embed/collegehumor',
  'core-embed/cloudup',
  'core-embed/animoto',
  'core-embed/vimeo',
  'core-embed/flickr',
  'core-embed/spotify',
  'core-embed/soundcloud',
  'core-embed/wordpress',
  //'core/embed',
  'core-embed/crowdsignal',
  'core-embed/speaker-deck'
]

window.onload = function() {
  disabledBlocks.map(function(block){
    wp.blocks.unregisterBlockType( block );
  });
}
