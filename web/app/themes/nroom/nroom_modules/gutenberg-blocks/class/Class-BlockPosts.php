<?php
class BlockPosts {
  public $block_id;
  public $posts_per_page;
  public $sum_of_post_on_page;
  static $instances=array();
  function __construct($block_id, $post_per_page, $sum_of_post_on_page){
    $this->block_id = $block_id;
    $this->post_per_page = $post_per_page;
    $this->sum_of_post_on_page = $sum_of_post_on_page;
    $this->posts_source = get_field('zrodlo_postow');
    $this->blocks_type_to_exclusion = get_post_blocks();
    if (is_archive() || is_home()) {
      $this->all_blocks_in_content = $this->search(parse_blocks(get_post(get_gutneberg_archive_template_id())->post_content ), 'blockName', '/acf\//');
    } else {
      $this->all_blocks_in_content = $this->search(parse_blocks( get_post(get_the_ID())->post_content ), 'blockName', '/acf\//' );
    }
    $this->exclusion_category = [];
    $this->exclusion_tags = [];
    $this->exclusion_posts = [];
    $this->offset = [];

    BlockPosts::$instances[] = $this;
  }

  /**
   * @param string $block_id
   * @return int number of post display in block
   */
  private function get_post_per_page_in_block($block_id) {
    foreach (BlockPosts::$instances as $block) {
      if ($block->block_id == $block_id) {
        return $block->post_per_page;
        break;
      }
    }
  }

  private function search($array, $key, $value) { 
    $results = array(); 
  
    // if it is array 
    if (is_array($array)) { 
  
      // if array has required key and value 
      // matched store result  
      if (isset($array[$key]) && preg_match($value, $array[$key], $matches)) { 
        $results[] = $array; 
      } 
  
      // Iterate for each element in array 
      foreach ($array as $subarray) {  
  
        // recur through each element and append result  
        $results = array_merge($results,  
        $this->search($subarray, $key, $value)); 
      } 
    } 
  
    return $results; 
  } 
  

  /**
   * Gdy user korzysta z tego samego zródła w roznych blokach na jednej stronie
   * przesówamy offset w kazdym kolejnym bloku z tym samym zródłem o sume postów 
   * z bloków wcześniejszych. Uzyskujemy dziękie temu automatyczne układanie postów na stronie
   * bez powtórzeń postów.
   * 
   * @param array $block Guteneberg
   * @return int liczba offset dla sprawdzanego bloku
   * 
   */
  public function get_offset($block) {
    $paged = get_query_var( 'paged', 1 );
    if ($paged == 0) {
      $page_offset = 0;
    } elseif ($paged == 2) {
      $page_offset = $this->sum_of_post_on_page;
    } else {
      $page_offset = ($paged - 1) * $this->sum_of_post_on_page;
    }  

    foreach ($this->all_blocks_in_content as $block_from_conent) {
      // var_dump($this->all_blocks_in_content);
      // die();

      $blocks_no_post = get_no_post_blocks();


      if (isset($block_from_conent["attrs"]["data"]) && !array_search( $block_from_conent["blockName"], $blocks_no_post)) { // bloki ktore nie sa stworzone przez ACF maja inne strukture danych
        
        $block_id = $block_from_conent["attrs"]["id"];
        $block_data = $block_from_conent["attrs"]["data"];
        $block_source = $block_from_conent["attrs"]["data"]["zrodlo_postow"];
        $block_post_type = $block_from_conent["attrs"]["data"]["typ_postow"];

        if (isset($block_from_conent["attrs"]["data"][$block_source."_id"])) {
          $taxonomy_id = $block_from_conent["attrs"]["data"][$block_source."_id"];
        } else {
          $taxonomy_id = 0;
        }

        $current_block_id = $block["id"];
        $current_block_source = $block["data"]["zrodlo_postow"];
        $current_block_post_type = $block["data"]["typ_postow"];

        if (isset($block["data"][$current_block_source."_id"])) {
          $current_taxonomy_id = $block["data"][$current_block_source."_id"];
        } else {
          $current_taxonomy_id = 0;
        }

        switch (true) {
          case (array_search('all', $block_data) || array_search('queried_object', $block_data)):
          case (array_search('category', $block_data) || array_search('post_tag', $block_data)) && $taxonomy_id == $current_taxonomy_id && $block_post_type == $current_block_post_type:

            // potrzebujemy sume wszystkich postów ze wspólnym źródłem by móc obliczyć offset
            $this->offset[$block_post_type][$taxonomy_id][] = $this->get_post_per_page_in_block($block_id);

            if (isset($block_id) && $block_id == $current_block_id) {


              $offset = array_sum($this->offset[$block_post_type][$taxonomy_id]) - end($this->offset[$block_post_type][$taxonomy_id]) + $page_offset;

              return $offset;

            } 

            break;
        }

      }

    }

  }

  /**
   * Wybierając źródło postów "Wszystkie" wykluczamy
   * uzyte w innych blokach kategorie, tagi i pojedyncze posty by uniknąć
   * duplikowania postów na jedenej stronie w blokach ze źródlem "Wszystkie"
   */
  private function get_used_posts() {

    foreach ($this->all_blocks_in_content as $block) {
      if ($block["blockName"] and in_array($block["blockName"], $this->blocks_type_to_exclusion)) {
        $block_source = $block["attrs"]["data"]["zrodlo_postow"];
        $block_data = $block["attrs"]["data"];
        if (isset($block_data[$block_source."_id"])) {
          $block_taxonomy_id = $block_data[$block_source."_id"];
        }

        switch ($block_source) {
          case 'category':
            $this->exclusion_category[] = $block_taxonomy_id;
            break;
          case 'post_tag':
            $this->exclusion_tags[] = $block_taxonomy_id;
            break;
          case 'exact_posts':
            for ($i = 0; $i <= $block_data["posty_sekcji"]; $i++) {
              if (isset($block_data["posty_sekcji_".$i."_post"])) {
                $this->exclusion_posts[] = $block_data["posty_sekcji_".$i."_post"];
              }
            }
            break;
        }
      } 
    }

    $used_posts["category"] = $this->exclusion_category;
    $used_posts["post_tag"] = $this->exclusion_tags;
    $used_posts["exact_posts"] = $this->exclusion_posts;
   
    return $used_posts;

  }

  /**
   * @param int $post_per_page number of post to get from source
   * @param int $offset number of offset for query
   * 
   * @return array 
   */
  public function get_posts_from_source($offset){
    // w szabloniu bloku uzywamy "source" do warunkowania tresci
    $vars['source'] = $this->posts_source;
    
    $used_posts = $this->get_used_posts();
    $posts_per_page = $this->post_per_page; 
    
    switch ($this->posts_source) {
      case 'all':
        $wp_query_args = [
          'category__not_in' => $used_posts["category"],
          'tag__not_in' => $used_posts["post_tag"],
          'offset' => $offset
        ];
        $vars['taxonomyLink'] = get_field('link_sekcji');
        break;
      case 'category':
        $wp_query_args = [
          'cat' => get_field('category_id'),
          'offset' => $offset
        ];
        $vars['taxonomyName'] = get_category(get_field('category_id'))->name;
        $vars['taxonomyLink'] = get_category_link(get_field('category_id'));
        break;
      case 'post_tag':
        $wp_query_args = [
          'tag_id' => get_field('post_tag_id'),
          'offset' => $offset
        ];
        $vars['taxonomyName'] = get_tag(get_field('post_tag_id'))->name;
        $vars['taxonomyLink'] = get_tag_link(get_field('post_tag_id'));
        break;
      case 'exact_posts':
        $chosen_posts = [];
        foreach ( get_field('posty_sekcji') as $choice){
          $chosen_posts[] = $choice['post'];
        }
        $wp_query_args = array(
          'post__in' => $chosen_posts,
          'posts_per_page' => $posts_per_page,
          'ignore_sticky_posts' => 1,
          'offset' => $offset,
          'orderby' => 'post__in'
        );
        $subtitles = [];
        foreach ( get_field('posty_sekcji') as $choice){
          if ( isset($choice['subtitle'])) {
            $subtitles[] = $choice['subtitle'];
          } else {
            $subtitles = null;
          }
        }
        $vars['subtitles'] = $subtitles;
        $vars['taxonomyLink'] = get_field('link_sekcji');
        break;
      case 'queried_object':
        if (is_author()) {
          $wp_query_args = array(
            'author' => get_queried_object()->ID,
            'offset' => $offset,
          );
        } elseif(is_tag()) { 
          $wp_query_args = array(
            'tag_id' => get_queried_object()->term_id,
            'offset' => $offset,
          );
        } elseif(is_category()) {
          $wp_query_args = array(
            'cat' => get_queried_object()->term_id,
            'offset' => $offset,
          );
        } else {
          $wp_query_args = array(
            'offset' => $offset,
          );
        }
        break;
    }
    $wp_query_args += [
      'post_type' => get_field('typ_postow'),
      'post__not_in' => $used_posts["exact_posts"],
      'ignore_sticky_posts' => true,
      'posts_per_page' => $posts_per_page,
      'post_status' => 'publish'
    ];
    return array(
      'wp_query_args' => $wp_query_args,
      'vars' => $vars
    );
  }   
}