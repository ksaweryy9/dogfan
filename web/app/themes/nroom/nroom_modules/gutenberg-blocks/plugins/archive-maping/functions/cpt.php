<?php
function register_cpts_archive_template() {

	/**
	 * Post Type: Archive Templates.
	 */

	$labels = array(
		"name" => __( "Archive Templates", "sage" ),
		"singular_name" => __( "Archive Template", "sage" ),
	);

	$args = array(
		"label" => __( "Archive Templates", "sage" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
    "show_in_nav_menus" => true,
    'menu_icon' => 'dashicons-grid-view',
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "archive_template", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "revisions", "author" ),
	);

	register_post_type( "archive_template", $args );
}

add_action( 'init', 'register_cpts_archive_template' );
