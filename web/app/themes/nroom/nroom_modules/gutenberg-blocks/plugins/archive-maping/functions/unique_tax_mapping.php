<?php
/**
 * User moze towrzyc i przypisywac szabloby stron archive ze wzgledu na post_tag lub category.
 * Jeśli wybrałbym dla jednej taksonomi dwa rózne szablony stworzył by konflikt dla tego zastała
 * zablokowana taka moliwosc i user do jendej taksonomi moze przypisac tylko jedna unikalną wartosc.
 */
add_filter('acf/validate_value/key=field_5cb5e9cf3609e', 'unique_repeater_sub_field', 20, 4);
add_filter('acf/validate_value/key=field_5cb5ea4a141c5', 'unique_repeater_sub_field', 20, 4);

function unique_repeater_sub_field($valid, $value, $field, $input) {
  if (!$valid) {
    return $valid;
  }
  
  // get list of array indexes from $input
  // [ <= this fixes my IDE, it has problems with unmatched brackets
  preg_match_all('/\[([^\]]+)\]/', $input, $matches);
  if (!count($matches[1])) {
    // this should actually never happen
    return $valid;
  }
  $matches = $matches[1];
  
  // walk the acf input to find the repeater and current row      
  $array = $_POST['acf'];
  
  $repeater_key = false;
  $repeater_value = false;
  $row_key = false;
  $row_value = false;
  $field_key = false;
  $field_value = false;
  
  for ($i=0; $i<count($matches); $i++) {
    if (isset($array[$matches[$i]])) {
      $repeater_key = $row_key;
      $repeater_value = $row_value;
      $row_key = $field_key;
      $row_value = $field_value;
      $field_key = $matches[$i];
      $field_value = $array[$matches[$i]];
      if ($field_key == $field['key']) {
        break;
      }
      $array = $array[$matches[$i]];
    }
  }
  
  if (!$repeater_key) {
    // this should not happen, but better safe than sorry
    return $valid;
  }
  
  // look for duplicate values in the repeater
  foreach ($repeater_value as $index => $row) {
    if ($index != $row_key && $row[$field_key] == $value) {
      // this is a different row with the same value
      $valid = 'this value is not unique';
      break;
    }
  }
  
  return $valid;
}
