<?php
function get_gutneberg_archive_template_id() {
  if (is_archive()) {
    if( have_rows('archive_mapping', 'options') ):
      $custom_archive_templates = [];
      while( have_rows('archive_mapping', 'options') ): the_row();
        $term_id = get_sub_field(get_sub_field('wybierz_typ_taksonomi'));
        $custom_archive_templates[$term_id] = get_sub_field('szablon_strony');
      endwhile;
    endif;
    if (isset($custom_archive_templates[get_queried_object()->term_id]) && $custom_archive_templates[get_queried_object()->term_id]) {
      $template_id = $custom_archive_templates[get_queried_object()->term_id];
    } else {
      $template_id = get_field('wybierz_szablon_domyslny', 'options');
    }
  
    return $template_id;
  } elseif (is_home()) {
    $template_id = get_field('wybierz_szablon_dla_strony_home', 'options');
    return $template_id;
  }

  
}
