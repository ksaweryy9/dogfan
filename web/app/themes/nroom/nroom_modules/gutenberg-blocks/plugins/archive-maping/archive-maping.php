<?php
add_filter('acf/settings/load_json', 'sw_gutneberg_plugin_archive_maping');
function sw_gutneberg_plugin_archive_maping( $paths ) {

  $paths[] = dirname(__FILE__) . '/acf-fields';

  return $paths;
}

if( function_exists('acf_add_options_page') ) {
 
	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Archive mapping',
		'menu_title' 	=> 'Archive mapping',
		'menu_slug' 	=> 'archive-mapping',
		'capability' 	=> 'edit_posts',
    'redirect' 	=> false,
    'position'  => 50,
    'icon_url'  => 'dashicons-list-view',
    'parent_slug' => 'edit.php?post_type=archive_template'
	));
 
}

foreach ( glob( __DIR__ . "/functions/*.php") as $filename ) {
  include $filename;
}
