<?php
function sw_wp_pagenavi($page, $url) {
  $post_per_page = get_sum_of_post_in_blocks(get_post(get_gutneberg_archive_template_id())->post_content);
  if (!is_author() && !is_search()) {
    if (is_category()) {
      $tax_id = get_queried_object()->term_id;
      $sum_of_post = get_category($tax_id)->count;
    } elseif (is_tag()) {
      $tax_id = get_queried_object()->term_id;
      $sum_of_post = get_tag($tax_id)->count;
    }
  } elseif(is_author()) {
    $sum_of_post = (int)count_user_posts(get_queried_object()->ID);
  }

  if (is_home() or is_search()) {
    $sum_of_post = wp_count_posts()->publish;
  }

  if (is_search()) {
    $post_per_page = 21;
  }

  $paged = $page;
  if ($page == 0) {
    $page = $page + 1;
  }
  $next_page = $page + 1;
  $back_page = $page - 1;
  if ($page == 2 ) {
    $back_link = $url;
  } else {
    $back_link = $url.'page/'.$back_page;
  }
  $next_link = $url.'page/'.$next_page;
  $back_links = '';

  if($post_per_page == 0) {
    $max_paged = 0;
  } else {
    $max_paged = round($sum_of_post / $post_per_page, 0, PHP_ROUND_HALF_UP);
  } 

  $last_link = $url.'page/'.$max_paged;

  if ($paged == 0) {
    $back_links = '';
    $next_html = '<div class="large-12 small-12 medium-12 columns"><a class="nextpostslink btn btn--big" rel="next" href="'.$next_link.'">Następna strona</a></div>';
    $page_number = '';
  } elseif ($paged == 2) {
    $back_links = '<div class="large-8 small-12 medium-12 columns"><a class="previouspostslink btn btn--big" rel="prev" href="'.$url.'">Wróć do strony głównej</a></div>';
    $next_html = '<div class="large-4 small-12 medium-12 columns"><a class="nextpostslink btn btn--big" rel="next" href="'.$next_link.'">Następna strona</a></div>';
    $page_number = '';
  } elseif ($max_paged>$page && !$paged == 0) {
    $back_links = '<div class="large-4 small-12 medium-12 columns"><a class="previouspostslink btn btn--big" rel="prev" href="'.$url.'">Wróć do strony głównej</a></div>';
    $next_html = '<div class="large-4 small-12 medium-12 columns"><a class="nextpostslink btn btn--big" rel="next" href="'.$next_link.'">Następna strona</a></div>';
    $page_number = '<div class="large-4 small-12 medium-12 columns"><div class="wp-pagenavi__numbers">
    <a href="'.$back_link.'" class="btn btn--big btn--nr">'.$back_page.'</a>
    <span class="current btn btn--big btn--nr">'.$page.'</span>
    <a href="'.$next_link.'" class="btn btn--big btn--nr">'.$next_page.'</a>
    <a href="'.$last_link.'" class="btn btn--big btn--nr last">'.$max_paged.'</a></div></div>';
  } elseif ($paged == $max_paged ) {
    $back_links = '<div class="large-4 small-12 medium-12 columns"><a class="nextpostslink btn btn--big" rel="next" href="'.$back_link.'">Porzednia strona</a></div>';
    $next_html = '<div class="large-8 small-12 medium-12 columns"><a class="previouspostslink btn btn--big" rel="prev" href="'.$url.'">Wróć do strony głównej</a></div>';
    $page_number = '';
  }

  $html = ''.
    $back_links
    .$page_number.
    $next_html
  .'';

  return $html;

}
