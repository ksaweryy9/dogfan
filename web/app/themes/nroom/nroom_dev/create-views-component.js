'use strict';

import gulp from 'gulp';
import fs from 'fs';
import arg from './fetch-comand-line-args';

gulp.task('create_views_component', () => {
  const toCamelCase = (str) => str.replace(/(?:^\w|[A-Z]|\b\w)/g, (ltr, idx) => idx === 0 ? ltr.toUpperCase() : ltr.toUpperCase()).replace(/\s+/g, '').replace(/-/g, '');

  const componentDir = 'views/components/';
  
  const fileStructure = [
    componentDir+arg.dir,
    componentDir+arg.dir+'/index.js',
    componentDir+arg.dir+'/_'+arg.dir+'.scss',
    componentDir+arg.dir+'/'+toCamelCase(arg.dir)+'.twig',
  ];

  const cssContent = '.'+arg.dir+' {\n\t@include themed() {\n\t\tcolor: t("title");\n\t}\n\}';

  const twigContent = '<div class="'+arg.dir+'">\n\t\n</div>';

  fileStructure.forEach(dir => {
    if (!fs.existsSync(dir)) {
      if (dir.indexOf('.') !== -1) {
        if (dir.indexOf('.scss') !== -1) {
          fs.writeFileSync(dir, cssContent);
        } else if (dir.indexOf('.twig') !== -1) {
          fs.writeFileSync(dir, twigContent);
        } else {
          fs.writeFileSync(dir, '');
        }
      } else {
        fs.mkdirSync(dir);
      }
      console.log('📁  folder created:', dir);
    } else {
      console.log('component exist');  
    }
  });
});