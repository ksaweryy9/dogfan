'use strict';

import gulp from 'gulp';
import fs from 'fs';
import arg from './fetch-comand-line-args';

gulp.task('create_gutneberg_block', () => {
  const toCamelCase = (str) => str.replace(/(?:^\w|[A-Z]|\b\w)/g, (ltr, idx) => idx === 0 ? ltr.toUpperCase() : ltr.toUpperCase()).replace(/\s+/g, '').replace(/-/g, '');
  const componentName = toCamelCase(arg.dir); 
  const componentDir = 'blocks/';
  const reactComponentDir = 'assets/js/react/blocks/';
  const comonentFolder = arg.dir;
  const functionName = arg.dir.replace('-', '_');

  /**
   * 
   */
  const fileStructure = [
    componentDir+comonentFolder,
    componentDir+comonentFolder+'/acf-fields',
    componentDir+comonentFolder+'/'+componentName+'.twig',
    (arg.posts ? componentDir+comonentFolder+'/'+arg.dir+'.php' : '' ),
    (arg.hub ? reactComponentDir+'/'+componentName+'.js' : '' ),
  ];

  /**
   * jesli blok ma byc typu hub musimy dodać "data-hub" oraz "data-blocktype" ktore są potrzebne
   * w nroom_modules/hub by function displayHub() mogła stworzyć odpowiednie componenty react.
   */
  const twigContent = '<div id="{{ block.id }}" class="'+comonentFolder+' {{ block.className }}"'+(arg.hub ? ' data-hub data-blocktype="'+componentName+'"' : '')+' data-easygut-id="'+comonentFolder.replace('-', '')+'">'+(arg.hub ? '' : '\n\t'+comonentFolder+'\n')+'</div>';

  /**
   * By przyśpieszyć pracę tworzymy w assets/js/react/block/ compnent bloku z difultowa zawartością.
   */
  const reactConent = 'import React from \'react\';\n\
import ReactDOM from \'react-dom\';\n\
import Skeleton from \'react-loading-skeleton\';\n\
\n\
class '+componentName+' extends React.Component {\n\
  constructor(props) {\n\
    super(props);\n\
    this.state = {\n\
    };\n\
  }\n\
  render() {\n\
    const { posts, isLoaded } = this.props;\n\
    return (\n\
      <div>\n\
        {(isLoaded) ? <Skeleton /> : <div>'+componentName+'</div>}\n\
      </div>\n\
    );\n\
  }\n\
}\n\
\n\
export default '+componentName+';';

  /**
   * jesli blok ma byc typu post musimy stworzyc go dodakowa funckja przez ACF (bez pliku .php blok jest tworzony 
   * autoatycznie tez ACF) oraz uzyc w callbacku fukcji z mroom_module/gutnebrg-blocks create_posts_block() oraz stowrzyc 
   * pole post_number_in_block która jest wykorzystywana do licznie offsety w class BlockPosts w nroom_modules/gutneberg-blocks. 
   */
  const phpContent = '<?php\n\
add_action("acf/init", "'+functionName+'_block");\n\
function '+functionName+'_block () {\n\
  if( function_exists("acf_register_block") ) {\n\
    acf_register_block(array(\n\
      "name" => "'+functionName+'",\n\
      "title" => __("'+functionName+'"),\n\
      "description" => __(""),\n\
      "render_callback" => "'+functionName+'_block_callback",\n\
      "category"=> "formatting",\n\
      "icon"=> "admin-comments",\n\
      "supports" => array(\n\
        "align" => false,\n\
      ),\n\
      //"keywords" => array( "testimonial", "quote" ),\n\
    ));\n\
  }\n\
}\n\
\n\
function '+functionName+'_block_callback( $block ) {\n\
  $posts_per_pege = get_field("post_numer_in_block");\n\
  $vars = [];\n\
  create_posts_block($block, $posts_per_pege, $vars, __DIR__);\n\
}\n\
\n\
if( function_exists("acf_add_local_field_group") ):\n\
\n\
  acf_add_local_field_group(array(\n\
    "key" => "group_".md5("'+functionName+'"),\n\
    "title" => "Liczba postów",\n\
    "fields" => array(\n\
      array(\n\
        "key" => "field_".md5("'+functionName+'"),\n\
        "label" => "Liczba postów w bloku",\n\
        "name" => "post_numer_in_block",\n\
        "type" => "number",\n\
        "instructions" => "",\n\
        "required" => 1, //chenge to 0 if block have constanst post_number_in_block\n\
        "readonly" => 0,\n\
        "conditional_logic" => 0,\n\
        "wrapper" => array(\n\
          "width" => "",\n\
          "class" => "",\n\
          "id" => "",\n\
        ),\n\
        "default_value" => "",  //put constanst post_number_in_block\n\
        "placeholder" => "",\n\
        "prepend" => "",\n\
        "append" => "",\n\
        "min" => "",\n\
        "max" => "",\n\
        "step" => "",\n\
      ),\n\
    ),\n\
    "location" => array(\n\
      array(\n\
        array(\n\
          "param" => "block",\n\
          "operator" => "==",\n\
          "value" => "acf/'+comonentFolder+'",\n\
        ),\n\
      ),\n\
    ),\n\
    "menu_order" => 0,\n\
    "position" => "normal",\n\
    "style" => "default",\n\
    "label_placement" => "top",\n\
    "instruction_placement" => "label",\n\
    "hide_on_screen" => "",\n\
    "active" => true,\n\
    "description" => "",\n\
  ));\n\
  \n\
endif;'; 

  const addSwitchCaseToNrmHubComponent = () => {
    const nrmHubDir = 'assets/js/react/components/NrmHub.js';
    fs.readFile(nrmHubDir, 'utf8', function(error, nrmHubFileOldContent) {
      if (error) {
        console.log('Error:- ' + error);
        throw error;
      }

      const switchCase = 'case "'+componentName+'":\n\
          return (\n\
            <ScrollTrigger onEnter={this.onEnterViewport.bind(this)} onExit={this.onExitViewport.bind(this)}>\n\
              <'+componentName+' posts={posts} fields={fields} isLoaded={isLoaded} />\n\
            </ScrollTrigger>\n\
          );\n\
          break;\n\
        default:';

      const componentImport = 'import '+componentName+' from \'../blocks/'+componentName+'\';\n\// auto import create_gutenberg_block';
      
      const nrmHubFileAddSwitch = nrmHubFileOldContent.replace(new RegExp('default:'), switchCase); 
      const nrmHubFileNewContent = nrmHubFileAddSwitch.replace(new RegExp('// auto import create_gutenberg_block'), componentImport); 
      fs.writeFileSync(nrmHubDir, nrmHubFileNewContent);
    });
  }

  fileStructure.forEach(dir => {
    if (!fs.existsSync(dir) && dir != '') {
      if (dir.indexOf('.') !== -1) {
        if (dir.indexOf('.twig') !== -1) {
          fs.writeFileSync(dir, twigContent);
        } else if (dir.indexOf('.php') !== -1) {
          fs.writeFileSync(dir, phpContent);
        } else if (dir.indexOf('.js') !== -1) {
          fs.writeFileSync(dir, reactConent);
          addSwitchCaseToNrmHubComponent();
        } else {
          fs.writeFileSync(dir, '');
        }
      } else {
        fs.mkdirSync(dir);
      }
      console.log('📁  folder created:', dir);
    } else {
      console.log('component exist');  
    }
  });
});