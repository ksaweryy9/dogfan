<?php
class CustomFields {
  public $file;

  function __construct($file){
    $this->file = $file;
  }

  /**
   * 
   * Funckja przeszukuje plik i znajduje "get_field('filed_name', $this->ID)" lub "get_field('hot_topic_interaction', 'option')" jeśli dodanym parametr "option".
   * Dodatkowo jeśli do funkjci get_field jest dodany obiekt jako 3 paramter np. "{"type": "image","instructions": "" }" do fielda dodawane są doatkowe opcje 
   * jak w przekazanym obiekcie. Jeśli funkjca get_field nie ma 3 parametru to tworzone pole przyjmuje domyslnie type = text.
   * 
   * ! "{"type": "image","instructions": "" }" nie moze miec spaacji za ","  (wrong: "{"type": "image", "instructions": "" }")
   * 
   * @param string{"option"} $type (optional)
   * @return array ACF fields like in acf_add_local_field_group() https://www.advancedcustomfields.com/resources/register-fields-via-php/
   * 
   */
  private function find_filds($type = '') {
    switch ($type) {
      case "option":
        $patern = "/get_field\(.+\'option'\)/";
        break;
      case "":
        $patern = '/(get_field\(.+\$this->ID\))|(get_field\(.+\$this->ID, \'{.+}\'\))/';
        break;
    }
    preg_match_all($patern, file_get_contents($this->file), $acf_fields);
    $acf_fields = array_unique($acf_fields[0]);
    $fields_options = [];
    foreach($acf_fields as $field) {
      $field_option = explode(', ', str_replace("'", '', str_replace(")", '', str_replace("get_field('", '', $field))));
      if (!isset($field_option[2])) {
        $field_option[2] = false;
      }
      $fields_options[] = $field_option;
    }

    $mergeId = 0;
    $remove_repeated_fields = array_reduce($fields_options, function($c, $x) use ($mergeId) {
      $c[$x[$mergeId]] = isset($c[$x[$mergeId]])
        ?array_combine(
          $z=array_keys($c[$x[$mergeId]]), 
          array_map(function($y) use ($x, $c, $mergeId) {
            return in_array($x[$y], (array)$c[$x[$mergeId]][$y])
              ?$c[$x[$mergeId]][$y]
              :array_merge((array)$c[$x[$mergeId]][$y], [$x[$y]]);
          }, $z)
        )
        :$x;
      return $c;
    }, []);
    
    $custom_fields = [];
    foreach ($remove_repeated_fields as $field_data) {
      $field = [
        'key' => 'field_' . uniqid(),
        'label' => ucfirst(str_replace("_", " ", $field_data[0])),
        'name' => $field_data[0]
      ];
      if ($field_data[2]) {
        $field_options = $field_data[2][0];
        foreach (json_decode($field_options) as $option_name => $option_value ) {
          $field[$option_name] = $option_value;
        }
      } else {
        $field["type"] = "text";
      }
      $custom_fields[] = $field;
    }

    return $custom_fields;
  }

  /**
   * @param array $fields
   */
  public function register_filds() {
    if( function_exists('acf_add_local_field_group') ) {
    }
  }

}
