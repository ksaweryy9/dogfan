<?php
// TODO funkcja do dopracowania
/**
 * Register ACF fields from twig files
 */
if (defined('WP_ENV') && WP_ENV !== 'production') {
  //add_action( 'after_setup_theme', 'register_acf_felds_from_twig_files' );
}
function register_acf_felds_from_twig_files() {
  $blocks = array_slice(scandir(BLOCKS_DIRECOTRY), 2);
  foreach ($blocks as $block) {
    $acf_block_fields = [];
    $block_name = $block;
    $block_dir = BLOCKS_DIRECOTRY.$block_name."/".ucfirst(dashesToCamelCase($block_name)).".twig";
    $block_content = file_get_contents($block_dir);
    preg_match('/data-easygut-id=".+"/', $block_content, $block_id);

    if($block_id) {
      preg_match_all("/{% include '.+' %}/", $block_content, $include_files);
      // var_dump($include_files[0]);
      // if($include_files[0]) {
      //   foreach($include_files[0] as $file ) {
      //     var_dump($file);
      //   }
      // }
      $block_id = str_replace('"', '', str_replace('data-easygut-id="', '', $block_id));
      $block_id = $block_id[0];
      preg_match_all('/{{ fields\..+}}/', $block_content, $block_fields);

      preg_match_all('/({% for.+ %}(\n+.+[^%]+)+{% endfor %})/', file_get_contents($block_dir), $block_repeater_fields);
      foreach($block_repeater_fields[0] as $repeater_field) {
        preg_match('/fields\.([a-zA-Z]+)/', $repeater_field, $field_name);
        if($field_name) {
          preg_match('/{% for (.+) in/', $repeater_field, $subfield_prefix);
          preg_match_all('/{{ '.$subfield_prefix[1].'\.(.+) }}/', $repeater_field, $subfield_names);
          $acf_sibfield_fields = [];
          foreach($subfield_names[1] as $subfiled_name) {
            $acf_sibfield_fields[] = [
              'key' =>  'field_' . uniqid(),
              'label' => $subfiled_name,
              'name' => $subfiled_name,
              'type' => 'text',
            ];
          }
          $acf_block_fields[] = [
            'key' =>  'field_' . uniqid(),
            'label' => $field_name[1],
            'name' => $field_name[1],
            'type' => 'repeater',
            'sub_fields' => $acf_sibfield_fields
          ];
          //var_dump($repeater_field, $field_name[1],  $subfield_names[1]);
        }
      }

      if($block_fields[0]) {
        $block_fields = $block_fields[0];
        foreach($block_fields as $field) {
          $field_name = str_replace(' }}', '', str_replace('{{ fields.','',$field));
          $acf_block_fields[] = array (
            'key' =>  'field_' . uniqid(),
            'label' => $field_name,
            'name' => $field_name,
            'type' => 'text',
          );
        }
      }

      $block_filds_group = array(
        'key' => 'easygut_'.$block_id,
        'title' => 'block: '.$block_id,
        'fields' => $acf_block_fields,
        'location' => array (
          array (
            array (
              'param' => 'block',
              'operator' => '==',
              'value' => 'acf/'.$block_id,
            ),
          ),
        ),
      );
      $file = file_put_contents( BLOCKS_DIRECOTRY . strtolower($block_name). '/' . 'acf-fields/' . $block_filds_group['key'] . '.json', json_encode($block_filds_group) );
      
    } else {
      if(is_admin()){
        $class = 'notice notice-error';
        $message = __( 'missing data-easygut-id="'.strtolower($block_name).'" in '. $block );
        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
      }
    }
  }
}