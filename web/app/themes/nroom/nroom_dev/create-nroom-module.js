'use strict';

import gulp from 'gulp';
import fs from 'fs';
import shell from 'gulp-shell';
const Bitbucket = require('bitbucket');
import arg from './fetch-comand-line-args';

gulp.task('create_nroom_module', () => {
  const basicFile = [
    'nroom_modules/'+arg.dir,
    'nroom_modules/'+arg.dir+'/acf-fields',
    'nroom_modules/'+arg.dir+'/index.js',
    'nroom_modules/'+arg.dir+'/'+arg.dir+'.scss',
    'nroom_modules/'+arg.dir+'/functions',
    'nroom_modules/'+arg.dir+'/'+arg.dir+'.php'
  ];

  const nodeModuleFile = [
    'nroom_modules/'+arg.dir+'/package.json',
    'nroom_modules/'+arg.dir+'/.gitignore'
  ];

  const mainFileContnet = '<?php\n\
  add_filter("acf/settings/load_json", "sw_'+arg.dir+'_acf_json_load_point");\n\
  function sw_'+arg.dir+'_acf_json_load_point( $paths ) {\n\
    $paths[] = dirname(__FILE__) . "/acf-fields";\n\
    return $paths;\n\
  }\n\
  \n\
  foreach (glob( __DIR__ . "/functions/*.php") as $filename) {\n\
    include $filename;\n\
  }\n\
  ';

  if (arg.node_module) {
    var fileStructure = basicFile.concat(nodeModuleFile);
  } else {
    var fileStructure = basicFile;
  }

  const bitbucket = new Bitbucket()

  function createPackagejson(dir, arg, callback) {
    fs.writeFileSync(dir, '{\n\t"name": "'+arg.dir+'",\n\t"version": "0.0.1",\n\t"description": ""\n}');
    callback();
  }

  function createFileStructure(files, callback) {
    files.forEach(dir => {
      if (!fs.existsSync(dir)) {
        if (dir.indexOf('.') !== -1) {
          if (dir.indexOf('.php') !== -1) {
            fs.writeFileSync(dir, mainFileContnet);
          } else if (dir.indexOf('.gitignore') !== -1) {
            fs.writeFileSync(dir, '.DS_Store\nnode_modules');
          } else if (dir.indexOf('package.json') !== -1) {
            createPackagejson(dir, arg, function() {
              return gulp.src('*.js', {read: false})
              .pipe(shell([
                'npm install --save ./nroom_modules/'+arg.dir
              ]));
            });
          }else {
            fs.writeFileSync(dir, '');
          }
        } else {
          fs.mkdirSync(dir);
        }
        console.log('📁  folder created:', dir);
      } else {
        console.log('module exist');  
      }
    });
    callback();
  }

  function createModules(repoFullName) {
    createFileStructure(fileStructure, function () {
      return gulp.src('*.js', {read: false})
      .pipe(shell([
        'cd nroom_modules/'+arg.dir+' && git init && git add --all && git commit -m "Initial Commit" && git remote add origin git@bitbucket.org:'+repoFullName+'.git && git push -u origin master && cd ../../../../../.. && git submodule add git@bitbucket.org:'+repoFullName+'.git web/app/themes/nroom/nroom_modules/'+arg.dir
      ]));
    });
  }

  if (!fs.existsSync(fileStructure[0])) {
    bitbucket.authenticate({
      type: 'basic',
      username: 'lukbieniek',
      password: 'kVYvnG8gYcjZSrnzJE9S'
    })
    bitbucket.repositories
      .create({ username: 'lukbieniek', repo_slug: arg.dir })
      .then(({ data, headers }) => createModules(data.full_name))
      .catch(err => console.error(err))
  }
});