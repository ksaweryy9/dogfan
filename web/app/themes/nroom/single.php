<?php
$context = Timber::get_context();
$post = Timber::query_post('BBPost');
$context['post'] = $post;
$args = array(
    'posts_per_page' => 2,
    'post_type' => 'post',
    'post__not_in' => array( get_the_ID() ),
    'orderby' => 'rand'
);
$context["commentReplyArgs"] = array('reply_text' => "Reply", 'depth' => 1, 'max_depth' => 5);
$context['all_posts'] = Timber::query_posts($args, 'ThemePost');
$post_id = ( $post_id ) ? $post_id : get_the_id();
$avg_rating = Rate_My_Post_Common::get_average_rating( $post_id );
$vote_count = Rate_My_Post_Common::get_vote_count( $post_id );
$context['rating'] = $avg_rating;
$context['count'] = $vote_count;
Timber::render( 'views/templates/single.twig', $context );