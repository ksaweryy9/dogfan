import React from 'react'
import ReactDOM from 'react-dom'
import Skeleton from 'react-loading-skeleton'
import PostRow from '../components/post-row/PostRow'
import PostTitle from '../components/post-title/PostTitle'

class SpecialPosts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    const { posts, isLoaded, fields } = this.props;
    return (
      <div className="special-posts" style={{backgroundColor: fields.bg_color}}>
        <div className="container">
          <div className="wp-block-columns">
            <div className="wp-block-column" style={{flexBasis: "66.66%"}}>
              <PostTitle post={fields} is_single={false} className="white big" />
              {(isLoaded) ? <div className=""><Skeleton height={200} /><Skeleton height={200} /></div> : <div className="">{posts.map(post => (<PostRow post={post.post} is_single={false} className="white" />))}</div>} 
            </div>
            <div className="wp-block-column" style={{flexBasis: "33.33%"}}></div>
          </div>
        </div>
      </div>
    );
  }
}

export default SpecialPosts;