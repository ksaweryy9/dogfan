import React from "react"
import ScrollBox from "../components/scroll-box/ScrollBox"
import SectionTitle from '../components/section-title/SectionTitle'

class ScrollPosts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { posts, isLoaded, fields } = this.props;
    const containerMargin = document.getElementById("container").offsetLeft;
    return(
      <div>
        <div style={{marginLeft: containerMargin}}>
          <SectionTitle fields={fields} />
        </div>
        <ScrollBox posts={posts} fields={fields} isLoaded={isLoaded} />
      </div>
    );
  }
}

export default ScrollPosts;