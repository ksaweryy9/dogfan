import React from 'react';
import ReactDOM from 'react-dom';
import Skeleton from 'react-loading-skeleton';
import PostTitle from '../components/post-title/PostTitle'
import ScrollBox from '../components/scroll-box/ScrollBox'

class PremiumPosts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    const { posts, isLoaded, fields } = this.props;
    return (
      <div className="premium-posts" style={{backgroundColor: "#171717"}}>
        <div className="container">
         <PostTitle post={fields} is_single={false} className="premium big" />
        </div>
        <ScrollBox posts={posts} fields={fields} isLoaded={isLoaded} />
      </div>
    );
  }
}

export default PremiumPosts;