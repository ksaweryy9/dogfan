import React from 'react';
import ReactDOM from 'react-dom';
import Skeleton from 'react-loading-skeleton';
import PostTitle from '../components/post-title/PostTitle';

class BigTitle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    const { fields, isLoaded } = this.props;
    return (
      <div className="big-title container">
        {(isLoaded) ? <Skeleton /> : <PostTitle post={fields} /> }
      </div>
    );
  }
}

export default BigTitle;