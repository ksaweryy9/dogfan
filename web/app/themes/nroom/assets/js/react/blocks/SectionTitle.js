import React from "react";
import ReactDOM from 'react-dom';
import Skeleton from "react-loading-skeleton";

class SectionTitle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  
  render() {
    const { fields, isLoaded } = this.props;
    return(
      (isLoaded) ? <div style={{marginBottom:17}}><Skeleton height={28} /></div> : <div className="section-title"><span className="section-title__name">{fields.title}</span></div>
      //(isLoaded) ? <div style={{height:48}}></div> : <div className="section-title"><span className="section-title__name">{fields.title}</span></div>
      //(isLoaded) ? <div></div> : <div className="section-title"><span className="section-title__name">{fields.title}</span></div>
    )
  }
}

export default SectionTitle;