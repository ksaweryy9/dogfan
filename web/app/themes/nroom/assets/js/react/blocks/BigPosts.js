import React from 'react';
import ReactDOM from 'react-dom';
import Skeleton from "react-loading-skeleton";
import PostBoks from '../components/post-boks/PostBoks';
import SectionTitle from '../components/section-title/SectionTitle';
import Wall from '../components/wall/Wall';

class BigPosts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { posts, fields, isLoaded } = this.props;
    const fieldsPolecane = {title: "Polecane"};
    const fieldsNajnowsze = {title: "Najnowsze"};
    return (
      <div className="big-posts container">
        <div className="wp-block-columns">
          <div className="wp-block-column" style={{flexBasis: "66.66%"}}>
            <SectionTitle fields={fieldsPolecane} />
            <div className="big-post">
              <div className="post-grid">
                {(isLoaded) ? <Skeleton /> : <PostBoks post={posts[0].post} is_single={false} />}
                {(isLoaded) ? <Skeleton /> : <PostBoks post={posts[1].post} is_single={false} />}
                {(isLoaded) ? <Skeleton /> : <PostBoks post={posts[2].post} is_single={false} />}
              </div>
            </div>
          </div>
          <div className="wp-block-column" style={{flexBasis: "33.33%"}}>
            <SectionTitle fields={fieldsNajnowsze} />
            <Wall />
          </div>
        </div>
      </div>
    );
  }
}

export default BigPosts;