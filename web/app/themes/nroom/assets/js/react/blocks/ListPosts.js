import React from 'react';
import ReactDOM from 'react-dom';
import Skeleton from 'react-loading-skeleton';
import PostRow from '../components/post-row/PostRow';
import SectionTitle from '../components/section-title/SectionTitle'

class ListPosts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  render() {
    const { posts, isLoaded, fields } = this.props;
    return (
      <div className="container">
        <div className="wp-block-columns">
          <div className="wp-block-column" style={{flexBasis: "66.66%"}}>
            <SectionTitle fields={fields} />
            {(isLoaded) ?
              <div>
                <div className="post-row">
                  <div className="post-row__img">
                    <Skeleton width={"100%"} height={200} />
                  </div>
                  <div className="post-row__content medium" style={{width: "100%", paddingTop: 10}}>
                    <Skeleton height={20} />
                    <Skeleton height={20} />
                    <Skeleton height={20} />
                  </div>
                </div>
                <div className="post-row">
                  <div className="post-row__img">
                    <Skeleton width={"100%"} height={200} />
                  </div>
                  <div className="post-row__content medium" style={{width: "100%", paddingTop: 10}}>
                    <Skeleton height={20} />
                    <Skeleton height={20} />
                    <Skeleton height={20} />
                  </div>
                </div>
                <div className="post-row">
                  <div className="post-row__img">
                    <Skeleton width={"100%"} height={200} />
                  </div>
                  <div className="post-row__content medium" style={{width: "100%", paddingTop: 10}}>
                    <Skeleton height={20} />
                    <Skeleton height={20} />
                    <Skeleton height={20} />
                  </div>
                </div>
              </div> : 
              <div className="">{posts.map(post => (<PostRow post={post.post} is_single={false} />))}</div>
            } 
          </div>
          <div className="wp-block-column" style={{flexBasis: "33.33%"}}></div>
        </div>
      </div>
    );
  }
}

export default ListPosts;