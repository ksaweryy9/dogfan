import React from "react";
import sectionSectionTitle from "../section-title/SectionTitle";
import postPostBoks from "../post-boks/PostBoks"

const Hero = (props) => {

const { fields,post } = props;



return(

<div className="hero row">
	<div className="columns large-8">
    <SectionTitle fields={fields}/>
    <div id="big-post"></div>

    <PostBoks post={post} is_single={is_single}/>

    <div className="post-grid half">
      <PostBoks post={post} is_single={is_single}/>
      <PostBoks post={post} is_single={is_single}/>
    </div>
  </div>
	<div className="columns large-4">
    <SectionTitle fields={fields}/>
    <div id="nroom-hub"></div>
    
  </div>
</div>
)

}

export default Hero;