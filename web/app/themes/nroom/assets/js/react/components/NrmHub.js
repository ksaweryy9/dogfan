import React from 'react';
import ReactDOM from 'react-dom';
import ScrollTrigger from 'react-scroll-trigger';
import BigPosts from '../blocks/BigPosts';
import ScrollPosts from '../blocks/ScrollPosts';
import SectionTitle from '../blocks/SectionTitle';
import BigTitle from '../blocks/BigTitle';
import ListPosts from '../blocks/ListPosts';
import SpecialPosts from '../blocks/SpecialPosts';
import PremiumPosts from '../blocks/PremiumPosts';
// auto import create_gutenberg_block

class NrmHub extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      blockId: false,
      blockType: null,
      viwied: false,
      isLoaded: true,
      posts: {},
      fields: {}
    };
  }

  componentDidMount() {
    this.setState({
      blockId: ReactDOM.findDOMNode(this).parentNode.getAttribute("id"),
      blockType: ReactDOM.findDOMNode(this).parentNode.getAttribute("data-blocktype"),
    });
  }

  onEnterViewport() {
    if (this.state.viwied === false ) {
      fetch(page_url+"/wp-json/hub/v2/"+this.state.blockId)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              isLoaded: false,
              posts: result.posts,
              fields: result.fields
            });
          },
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
    }
  }

  onExitViewport() {
    this.setState({
      viwied: true
    });
  }
  
  render() {
    const { posts, isLoaded, blockId, blockType, fields } = this.state;
    if (blockId != false && blockType != null) {
      switch(blockType){
        case 'BigPosts':
          return (
            <ScrollTrigger onEnter={this.onEnterViewport.bind(this)} onExit={this.onExitViewport.bind(this)}>
              <BigPosts posts={posts} fields={fields} isLoaded={isLoaded} />
            </ScrollTrigger> 
          );
          break; 
        case 'ScrollPosts':
          return (
            <ScrollTrigger onEnter={this.onEnterViewport.bind(this)} onExit={this.onExitViewport.bind(this)}>
              <ScrollPosts posts={posts} fields={fields} isLoaded={isLoaded} />
            </ScrollTrigger> 
          );
          break; 
        case 'SectionTitle':
          return (
            <ScrollTrigger onEnter={this.onEnterViewport.bind(this)} onExit={this.onExitViewport.bind(this)}>
              <SectionTitle fields={fields} isLoaded={isLoaded} />
            </ScrollTrigger>
          ); 
          break; 
        case "BigTitle":
          return (
            <ScrollTrigger onEnter={this.onEnterViewport.bind(this)} onExit={this.onExitViewport.bind(this)}>
              <BigTitle posts={posts} fields={fields} isLoaded={isLoaded} />
            </ScrollTrigger>
          );
          break;
          return (
            <ScrollTrigger onEnter={this.onEnterViewport.bind(this)} onExit={this.onExitViewport.bind(this)}>
              <ListPosts posts={posts} fields={fields} isLoaded={isLoaded} />
            </ScrollTrigger>
          );
          break;
        case "ListPosts":
          return (
            <ScrollTrigger onEnter={this.onEnterViewport.bind(this)} onExit={this.onExitViewport.bind(this)}>
              <ListPosts posts={posts} fields={fields} isLoaded={isLoaded} />
            </ScrollTrigger>
          );
          break;
        case "SpecialPosts":
          return (
            <ScrollTrigger onEnter={this.onEnterViewport.bind(this)} onExit={this.onExitViewport.bind(this)}>
              <SpecialPosts posts={posts} fields={fields} isLoaded={isLoaded} />
            </ScrollTrigger>
          );
          break;
        case "PremiumPosts":
          return (
            <ScrollTrigger onEnter={this.onEnterViewport.bind(this)} onExit={this.onExitViewport.bind(this)}>
              <PremiumPosts posts={posts} fields={fields} isLoaded={isLoaded} />
            </ScrollTrigger>
          );
          break;
        default:
      }
    } else {
      return(<div></div>);
    }
  }
}

export default NrmHub;