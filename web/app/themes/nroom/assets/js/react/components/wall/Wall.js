import React from "react";

const Wall = (props) => {

return(

	<div className="wall">
    <div className="wall__item">
      <div className="wall__item__date">14:43</div>
      <div className="wall__item__title">MUSSO – nowy pick-up marki SsangYong</div>
    </div>

    <div className="wall__item">
      <div className="wall__item__date">14:43</div>
      <div className="wall__item__title">Grupa Elcamp zaprezentuje kamper 4x4</div>
      <div className="wall__item_lebels">
        <div className="label label--hot">Nie przegap</div>
      </div>
    </div>

    <div className="wall__item wall__item--hot">
      <div className="ring-container">
        <div className="ringring"></div>
        <div className="circle"></div>
      </div>
      <div className="wall__item__date">14:43</div>
      <div className="wall__item__title">VOLVO TALKS na Poznań Motor Show!</div>
    </div>

    <div className="wall__item">
      <div className="wall__item__date">14:43</div>
      <div className="wall__item__title">Nowe modele i technologie Toyoty na Poznań Motor Show</div>
    </div>
  </div>
)

}

export default Wall;