import React from "react";
import darkindex from "./dark-mode/index";
import index from "./search/index"

const header = (props) => {

const { site,menu_header,item,slide_menu,search_form  } = props;



return(

<header className="header">
  <div className="container">
    <div className="header__logo">
      <a className="hide-theme-dark" href={ site.url } aria-label="Home">
        {% include 'assets/images/logo.svg' %}
      </a>
      <a className="hide-theme-default" href={ site.url } aria-label="Home">
        {% include 'assets/images/logo-dm.svg' %}
      </a>
      <index search_form ={search_form }/>
    </div>
    <div className="header__menu">

      <div className="header__menu__items show-for-large">
        {menu_header.items.map((item, index ) => (
          <a className="{{ item.classNamees|join(' ') }}" href={ item.link }>{ item.title }</a>
        ))}
      </div>

      <index search_form ={search_form }/>

      <div className="header__secend-menu nav-button hide-for-large" aria-label="Swipe menu">
        <button className="nav-button button-lines button-lines-x close">
          <span className="lines"></span>
        </button>
      </div>
    </div>
  </div>
</header>

<nav className='swipe-menu'>
  {slide_menu.items.map((item, index ) => (
    <a className="{{ item.classNamees|join(' ') }}" href={ item.link }>{ item.title }</a>
  ))}
</nav>
)

}

export default header;