import React from "react";
import PostTitle from "../post-title/PostTitle";
import PostAuthor from "../post-author/PostAuthor"

const PostRow = (props) => {

const { post,is_single, className } = props;

function createContent() { return {__html: post.nrm_comments_btn} }; 

return(
  <div className={`post-row ${className}`}>
    
    <div className="post-row__img">
      <div className="post-row__labels">
        <div dangerouslySetInnerHTML={createContent()} />
      </div>
      <img src={post.nrm_image_src.oryginal} alt=""/>
    </div>
    
    <div className="post-row__content medium">
      <PostTitle is_single={is_single} post={post}/>
      <PostAuthor post={post}/>
    </div>

  </div>
)

}

export default PostRow;