import React from "react";


const footer = (props) => {

const { 1,i ,attribute(_context, 'footer_'~loop,item } = props;



return(

<footer className="footer">
  <div className="row large-collapse medium-collapse small-collapse">
    <div className="footer__info large-6 small-12 medium-12 column">
      <div className="footer__logo">
        <div className="hide-theme-dark">
          <svg width="42" height="53" viewBox="0 0 42 53" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0V42.4063H7.875V52.6766L18.0469 42.4063H30.8438L22.9688 34.4551H7.875V7.95119H34.125V34.4551H30.8438V42.4063H42V0H0Z" fill="#9F0060"/>
            <g opacity="0.5">
            <g opacity="0.5">
            <path opacity="0.5" d="M18.0469 42.4062L7.875 47.1273V42.4062H18.0469Z" fill="black"/>
            </g>
            </g>
          </svg>
        </div>        
        <div className="hide-theme-default">
          <svg width="42" height="53" viewBox="0 0 42 53" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 0V42.4063H7.875V52.6766L18.0469 42.4063H30.8438L22.9688 34.4551H7.875V7.95119H34.125V34.4551H30.8438V42.4063H42V0H0Z" fill="#FFFFFF"/>
            <g opacity="0.5">
            <g opacity="0.5">
            <path opacity="0.5" d="M18.0469 42.4062L7.875 47.1273V42.4062H18.0469Z" fill="black"/>
            </g>
            </g>
          </svg>
        </div>        
      </div>
      <div className="footer__txt">
        <div className="footer__title show-for-large">2019 Spider’s Web. WSZELKIE PRAWA ZASTRZEŻONE</div>
        <div className="footer__title footer__title--noimportant hide-for-large">Ważne: wykorzystujemy pliki</div>
        <div className="footer__cookies">Używamy informacji zapisanych za pomocą cookies i podobnych technologii m.in. w celach reklamowych i statystycznych oraz w celu dostosowania naszych serwisów do indywidualnych potrzeb użytkowników.</div>
        <div className="footer__partners">
          <div className="footer__partners__item">Hosting: <a href="https://oktawave.com/">Oktawave</a></div>
          <div className="footer__partners__item">Support: <a href="http://spiders.agency/">Spiders.Agency</a></div>
        </div>
      </div>
    </div>
    <div className="footer__social hide-for-large">
      <a href="">
        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M30 3.21429V26.7857C30 28.5603 28.5603 30 26.7857 30H21.0737V18.1339H25.1317L25.7143 13.6071H21.0737V10.7143C21.0737 9.40179 21.4353 8.51116 23.317 8.51116H25.7143V4.46652C25.2991 4.41295 23.8795 4.28571 22.2187 4.28571C18.7634 4.28571 16.3929 6.39509 16.3929 10.2723V13.6138H12.3214V18.1406H16.3996V30H3.21429C1.43973 30 0 28.5603 0 26.7857V3.21429C0 1.43973 1.43973 0 3.21429 0H26.7857C28.5603 0 30 1.43973 30 3.21429Z" fill="#C6C6C6"/>
        </svg>
      </a>
      <a href="">
        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M15.0033 7.30838C10.7468 7.30838 7.31344 10.7425 7.31344 15C7.31344 19.2575 10.7468 22.6916 15.0033 22.6916C19.2599 22.6916 22.6933 19.2575 22.6933 15C22.6933 10.7425 19.2599 7.30838 15.0033 7.30838ZM15.0033 20.0006C12.2527 20.0006 10.0039 17.758 10.0039 15C10.0039 12.242 12.246 9.99944 15.0033 9.99944C17.7607 9.99944 20.0028 12.242 20.0028 15C20.0028 17.758 17.754 20.0006 15.0033 20.0006ZM24.8015 6.99375C24.8015 7.99119 23.9983 8.7878 23.0078 8.7878C22.0106 8.7878 21.2142 7.98449 21.2142 6.99375C21.2142 6.00301 22.0173 5.19971 23.0078 5.19971C23.9983 5.19971 24.8015 6.00301 24.8015 6.99375ZM29.8946 8.81457C29.7808 6.41136 29.232 4.28261 27.4718 2.52873C25.7184 0.774852 23.5901 0.225929 21.1874 0.105434C18.7111 -0.0351444 11.2889 -0.0351444 8.8126 0.105434C6.41662 0.219235 4.28834 0.768158 2.52817 2.52203C0.767987 4.27591 0.225878 6.40466 0.10541 8.80788C-0.0351366 11.2847 -0.0351366 18.7086 0.10541 21.1854C0.219186 23.5886 0.767987 25.7174 2.52817 27.4713C4.28834 29.2251 6.40993 29.7741 8.8126 29.8946C11.2889 30.0351 18.7111 30.0351 21.1874 29.8946C23.5901 29.7808 25.7184 29.2318 27.4718 27.4713C29.2253 25.7174 29.7741 23.5886 29.8946 21.1854C30.0351 18.7086 30.0351 11.2914 29.8946 8.81457ZM26.6955 23.843C26.1735 25.1551 25.1629 26.1659 23.8444 26.6947C21.8701 27.478 17.1852 27.2972 15.0033 27.2972C12.8215 27.2972 8.12995 27.4713 6.1623 26.6947C4.85053 26.1726 3.83993 25.1618 3.31121 23.843C2.52817 21.8682 2.70887 17.1823 2.70887 15C2.70887 12.8177 2.53486 8.12507 3.31121 6.15698C3.83324 4.84492 4.84384 3.8341 6.1623 3.30525C8.13664 2.52203 12.8215 2.70278 15.0033 2.70278C17.1852 2.70278 21.8767 2.52873 23.8444 3.30525C25.1562 3.8274 26.1668 4.83822 26.6955 6.15698C27.4785 8.13176 27.2978 12.8177 27.2978 15C27.2978 17.1823 27.4785 21.8749 26.6955 23.843Z" fill="#C6C6C6"/>
        </svg>
      </a>
      <a href="">
        <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M26.7857 0H3.21429C1.43973 0 0 1.43973 0 3.21429V26.7857C0 28.5603 1.43973 30 3.21429 30H26.7857C28.5603 30 30 28.5603 30 26.7857V3.21429C30 1.43973 28.5603 0 26.7857 0ZM23.5112 10.6339C23.5246 10.8214 23.5246 11.0156 23.5246 11.2031C23.5246 17.0089 19.1049 23.6987 11.029 23.6987C8.53795 23.6987 6.22768 22.9754 4.28571 21.7299C4.64062 21.7701 4.98214 21.7835 5.34375 21.7835C7.39955 21.7835 9.28795 21.0871 10.7946 19.9085C8.86607 19.8683 7.24554 18.6027 6.68973 16.8616C7.36607 16.9621 7.97545 16.9621 8.67188 16.7812C6.66295 16.3728 5.15625 14.6049 5.15625 12.4687V12.4152C5.73884 12.7433 6.42187 12.9442 7.13839 12.971C6.536 12.5703 6.04218 12.0267 5.70098 11.3887C5.35979 10.7507 5.18185 10.0382 5.18304 9.31473C5.18304 8.49777 5.39732 7.74777 5.77902 7.09821C7.94196 9.76339 11.1897 11.5045 14.8326 11.692C14.2098 8.71205 16.4397 6.29464 19.1183 6.29464C20.3839 6.29464 21.5223 6.82366 22.3259 7.6808C23.317 7.4933 24.2679 7.125 25.1116 6.62277C24.7835 7.64062 24.0938 8.49777 23.183 9.04018C24.067 8.94643 24.9241 8.69866 25.7143 8.35714C25.1183 9.23438 24.3683 10.0112 23.5112 10.6339Z" fill="#C6C6C6"/>
        </svg>
      </a>
    </div>
    <div className="footer__menus large-6 small-12 medium-12 column">
      {1..3.map((i, index ) => (
        <div className="footer__menu footer__menu_undefined>
          <div className="footer__title">{{ attribute(_context, 'footer_'~loop.index).title }}</div>
          <ul className="footer__menu__list footer__list">
              {attribute(_context, 'footer_'~loop.index).items.map((item, index ) => (
                <li><a className="footer__link" href={ item.link }>{ item.title }</a></li>
              ))}
          </ul>
        </div>
      ))}
    </div>
  </div>
</footer>
)

}

export default footer;