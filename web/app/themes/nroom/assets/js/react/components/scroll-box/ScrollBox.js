import React from "react";
import Skeleton from "react-loading-skeleton"
import PostBoks from '../post-boks/PostBoks'


class ScrollBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { posts, isLoaded, fields } = this.props;
    const containerMargin = document.getElementById("container").offsetLeft;
    return(
      <div className="scroll-box">
        <div className="scroll-box__arrows" style={{marginLeft: containerMargin}}>
          <div className="scroll-box__arrow scroll-box__arrow-right">
            <svg width="7" height="19" viewBox="0 0 7 19" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M6.84766 9.81538L1.49557 15.3076C1.32422 15.482 1.04714 15.482 0.875781 15.3076L0.153906 14.5728C-0.0174479 14.3984 -0.0174479 14.1164 0.153906 13.9419L4.47422 9.49995L0.153906 5.05796C-0.0174479 4.88355 -0.0174479 4.60151 0.153906 4.4271L0.875781 3.69233C1.04714 3.51792 1.32422 3.51792 1.49557 3.69233L6.84766 9.18452C7.01901 9.35894 7.01901 9.64097 6.84766 9.81538Z" fill="black"/>
            </svg>
          </div>
          <div className="scroll-box__arrow scroll-box__arrow-left unactive">
            <svg width="7" height="19" viewBox="0 0 7 19" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.152343 9.81538L5.50443 15.3076C5.67578 15.482 5.95286 15.482 6.12422 15.3076L6.84609 14.5728C7.01745 14.3984 7.01745 14.1164 6.84609 13.9419L2.52578 9.49995L6.84609 5.05796C7.01745 4.88355 7.01745 4.60151 6.84609 4.4271L6.12422 3.69233C5.95286 3.51792 5.67578 3.51792 5.50443 3.69233L0.152343 9.18452C-0.019011 9.35894 -0.019011 9.64097 0.152343 9.81538Z" fill="black"/>
            </svg>
          </div>
        </div>
        {(isLoaded) ? <div className="post-grid scroll" style={{marginLeft: containerMargin}}><Skeleton /><Skeleton /><Skeleton /><Skeleton /></div> : <div className="post-grid scroll" style={{marginLeft: containerMargin}}>{
          posts.map(post => (<PostBoks post={post.post} is_single={false} />))
        }</div>}
        
      </div>
    );
  }
}

export default ScrollBox;