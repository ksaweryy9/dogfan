import React from "react";


const PostTitle = (props) => {

const { is_single,post, className } = props;

function createContent() { return {__html: post.nrm_excerpt}; }

return(

<div className={className}>
  <div className="post-source">Żródło</div>
  <Choose><When condition={ is_single }>
    <h1 className="post-title">{ post.title }</h1>
  </When><Otherwise>
    <h2 className="post-title">{ post.title }</h2>
  </Otherwise></Choose>
  <div className="post-excerpt">
    <div dangerouslySetInnerHTML={createContent()} />
  </div>
</div>
)

}

export default PostTitle;