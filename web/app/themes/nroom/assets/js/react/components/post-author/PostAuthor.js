import React from "react";


const PostAuthor = (props) => {

const { post } = props;



return(

<div className="post-author">
  <div className="post-author__avatar">
    
  </div>
  <div>
    <div className="post-author__name">{ post.nrm_author_name }</div>
    <div className="post-author__subtitle">{ post.nrm_date }</div>
  </div>
</div>
)

}

export default PostAuthor;