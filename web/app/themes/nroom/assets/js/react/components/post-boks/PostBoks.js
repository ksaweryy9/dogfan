import React from "react";
import PostTitle from "../post-title/PostTitle";
import PostAuthor from "../post-author/PostAuthor"

const PostBoks = (props) => {

const { post,is_single } = props;

function createContent() { return {__html: post.nrm_comments_btn}; }

return(

<div className="post-boks" style={{ backgroundImage:`url(${ post.nrm_image_src.oryginal })` }}>
  <div className="medium white">
    <PostTitle is_single={is_single} post={post}/>
    <div className="post-info">
      <div dangerouslySetInnerHTML={createContent()} />
      <PostAuthor post={post}/>
    </div>
  </div>
</div>
)

}

export default PostBoks;