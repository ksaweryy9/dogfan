import React from "react";


const SectionTitle = (props) => {

const { fields } = props;



return(

<div className="section-title">
  <span>{ fields.title }</span>
</div>
)

}

export default SectionTitle;