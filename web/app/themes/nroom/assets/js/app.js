import $ from 'jquery';
import 'what-input';
import Router from './lib/router';

// Routes
import common from './routes/common';
import home from './routes/home';


window.$ = $;

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
  common,
  home
});

$("#menu-toggler").click(function(){
  $("#menu").toggleClass("active");
  
});

$('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (var i=0;i<3;i++) {
    next=next.next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
  }
});

$("#showPosts").click(function(){
  $("#showPosts").css({ display: "none" });
  $("#loading").css({ display: "flex" });
  setTimeout(function(){
    $("#all-posts").css({ display: "block" });
    //$("#eight-posts").css({ display: "none" });
    $("#hidePosts").css({ display: "inline-block" });
    $("#seeFull").css({ display: "inline-block" });
    $("#loading").css({ display: "none" });
  },1000);
  
});

/*
$('.sf-input-text').blur(function(){
  if(!$(this).val()){
      $('.sf-field-submit').removeClass("active");
  } else{
    $('.sf-field-submit').addClass("active");
  }
}); */

function checkForInput(element) {
  // element is passed to the function ^
  
  const $label = $(element).parentsUntil('.sf-field-search');

  if ($(element).val().length > 0) {
    $('.sf-field-submit').addClass('active');
    $('.search-box').addClass('active');
  } else {
    $('.sf-field-submit').removeClass('active');
    $('.search-box').removeClass('active');
  }
}

// The lines below are executed on page load
$('input.sf-input-text').each(function() {
  checkForInput(this);
});

// The lines below (inside) are executed on change & keyup
$('input.sf-input-text').on('change keyup', function() {
  checkForInput(this);  
});

const colors = ['#00AA55', '#009FD4', '#B381B3', '#939393', '#E3BC00', '#D47500', '#DC2A2A'];

function numberFromText(text) {
  // numberFromText("AA");
  const charCodes = text
    .split('') // => ["A", "A"]
    .map(char => char.charCodeAt(0)) // => [65, 65]
    .join(''); // => "6565"
  return parseInt(charCodes, 10);
}

const avatars = document.querySelectorAll('.avatar');

avatars.forEach(avatar => {
  const text = avatar.innerText; // => "AA"
  avatar.style.backgroundColor = colors[numberFromText(text) % colors.length]; // => "#DC2A2A"
});

const btn = document.getElementById('topButton');

btn.addEventListener('click', () => window.scrollTo({
  top: 0,
  behavior: 'smooth',
}));



/** Load Events */
$(document).ready(() => routes.loadEvents());
