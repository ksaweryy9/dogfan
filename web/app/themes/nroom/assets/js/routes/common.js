import $ from 'jquery';
import swLqip from 'sw-lqip';
import displayHub from 'hub';
import bootstrap from 'bootstrap';
import 'popper.js';
require('../../../views/components/dark-mode/index');

export default {
  init() {
    // JavaScript to be fired on all pages

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
