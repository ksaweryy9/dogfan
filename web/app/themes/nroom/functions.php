<?php
define('THEME_DIRECOTRY', dirname(__DIR__). '/nroom' );
define('BLOCKS_DIRECOTRY', THEME_DIRECOTRY . '/blocks/' );
require_once( __DIR__ . '/vendor/autoload.php' );

$timber = new \Timber\Timber();

foreach (glob( __DIR__ . "/nroom_dev/*.php") as $filename) {
  require_once $filename;
}

function search($array, $key, $value) { 
  $results = array(); 
  // if it is array 
  if (is_array($array)) { 
    // if array has required key and value 
    // matched store result  
    if (isset($array[$key]) && preg_match($value, $array[$key], $matches)) { 
      $results[] = $array; 
    } 
    // Iterate for each element in array 
    foreach ($array as $subarray) {  
      // recur through each element and append result  
      $results = array_merge($results,  
      search($subarray, $key, $value)); 
    } 
  } 
  return $results; 
}

function parse_acf_blocks($content) {
  return search(parse_blocks($content), 'blockName', '/acf\//');
}

function dashesToCamelCase($string, $capitalizeFirstCharacter = false) {
  $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
  if (!$capitalizeFirstCharacter) {
    $str[0] = strtolower($str[0]);
  }

  return $str;
}

$sage_includes = [
  'lib/timber.php',
  'lib/assets.php',
	'lib/setup.php',
	'lib/class-post.php',
	'nroom_modules/hub/hub.php'
];

foreach ( $sage_includes as $file ) {
  $filepath = locate_template( $file );
	if ( !$filepath  ) {
		trigger_error( sprintf( __( 'Error locating %s for inclusion', 'nroom' ), $file ), E_USER_ERROR );
	}

	require_once $filepath;
}

foreach (glob( __DIR__ . "/nroom_modules/*/*.php") as $filename) {
	if(!strpos($filename, 'hub.php')) {
		require_once $filename;
	}
}

foreach (glob( __DIR__ . "/blocks/*/*.php") as $filename) {
  require_once $filename;
}

function nroom_modules_path() {
	return get_stylesheet_directory_uri().'/nroom_modules\/';
}

function nroom_search_form( $form ) {
	$form = '<form role="search" method="get" id="searchform" class="search__form" action="' . home_url( '/' ) . '" >
	<svg class="search__close" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="szukaj" onfocus="this.placeholder = \'\'"onblur="this.placeholder = \'Zadaj pytanie, my odpowiemy i pokażemy najlepsze teksty\'" />
	<button type="submit" class="search__submit"><svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
	<path d="M9 17C13.4183 17 17 13.4183 17 9C17 4.58172 13.4183 1 9 1C4.58172 1 1 4.58172 1 9C1 13.4183 4.58172 17 9 17Z" stroke="#535353" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
	<path d="M19.0004 19.0004L14.6504 14.6504" stroke="#535353" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
	</svg></button>
	</form>';

	return $form;
}
add_filter( 'get_search_form', 'nroom_search_form' );

add_filter('pre_get_avatar_data', 'pre_get_avatar_data_cb', 10, 1);
function pre_get_avatar_data_cb($args){
	$args['size'] = 35;
	$args['width'] = 35;
	$args['height'] = 35;
	return $args;
}

add_image_size( 'post-boks', 380, 535, array( 'center', 'center' ) );
add_image_size( 'post-row', 280, 215, array( 'center', 'center' ) );

function get_acf_register_blocks() {
  $all_register_blocks = $block_types = WP_Block_Type_Registry::get_instance()->get_all_registered();
  $acf_blocks = [];
  foreach ($all_register_blocks as $block_name => $block_object) {
    if(strstr($block_name, "acf/")) {
      $acf_blocks[] = $block_name; 
    }
  }
  return $acf_blocks;
}

add_filter( 'allowed_block_types', 'misha_allowed_block_types', 10, 2 ); 
function misha_allowed_block_types( $allowed_blocks, $post ) {
   
  if( $post->post_type === 'post' ) {
    $allowed_blocks = array(
      'core/image',
      'core/paragraph',
      'core/heading',
      'core/list',
      'core/gallery',
      'core/quote',
      'core-embed/twitter',
      'core-embed/youtube',
      'core-embed/facebook',
      'core-embed/instagram',
	  'acf/polecane-karmy',
	  'acf/oceny',
    );
  }
 
	if( $post->post_type === 'page' ) {
		$allowed_core_blocks = array(
      'core/paragraph',
      'core/heading', 
    );
    $acf_blocks = get_acf_register_blocks();
    $allowed_blocks = array_merge($allowed_core_blocks, $acf_blocks);
  }
 
	return $allowed_blocks;
 
}

add_filter( 'upload_mimes', 'my_myme_types', 1, 1 );
function my_myme_types( $mime_types ) {
  $mime_types['webp'] = 'image/webp';
  return $mime_types;
}

add_action('wp_print_scripts', function(){
  if ( (!is_admin()) && is_singular() && comments_open() && get_option('thread_comments') ) wp_enqueue_script( 'comment-reply' );
});

function ea_comment_textarea_placeholder( $args ) {
	$args['comment_field']        = str_replace( 'textarea', 'textarea placeholder="Komentarz"', $args['comment_field'] );
	return $args;
}
add_filter( 'comment_form_defaults', 'ea_comment_textarea_placeholder' );

/**
 * Comment Form Fields Placeholder
 *
 */
function wpb_comment_reply_text( $link ) {
	$link = str_replace( 'Reply', 'odpowiedz', $link );
	return $link;
	}
add_filter( 'comment_reply_link', 'wpb_comment_reply_text' );


function be_comment_form_fields( $fields ) {
	foreach( $fields as &$field ) {
		$field = str_replace( 'id="author"', 'id="author" placeholder="Nazwa"', $field );
		$field = str_replace( 'id="email"', 'id="email" placeholder="E-mail"', $field );
		$field = str_replace( 'id="url"', 'id="url" placeholder="Witryna internetowa"', $field );
	}
	return $fields;
}
add_filter( 'comment_form_default_fields', 'be_comment_form_fields' );


function wpse53671_comment_form_before_fields() {
    echo '<div class="comment-form-fields">';
}
add_action( 'comment_form_before_fields', 'wpse53671_comment_form_before_fields' );

function wpse53671_comment_form_after_fields() {
    echo '</div>';
}
add_action( 'comment_form_after_fields', 'wpse53671_comment_form_after_fields' );

function wpse52737_enqueue_comment_reply_script() {
    if ( get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment_reply' );
    }
}
add_action( 'comment_form_before', 'wpse52737_enqueue_comment_reply_script' );

function remove_post_type_page_from_search() {
  global $wp_post_types;
  $wp_post_types['page']->exclude_from_search = true;
}
add_action('init', 'remove_post_type_page_from_search');

function cptui_register_my_cpts() {

	/**
	 * Post Type: Karmy Mokre.
	 */

	$labels = [
		"name" => __( "Karmy Mokre", "nroom" ),
		"singular_name" => __( "Karma Mokra", "nroom" ),
	];

	$args = [
		"label" => __( "Karmy Mokre", "nroom" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "karmy_mokre", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "excerpt", "trackbacks", "custom-fields", "comments", "revisions", "author", "page-attributes", "post-formats" ],
	];

	register_post_type( "karmy_mokre", $args );

	/**
	 * Post Type: Karmy Suche.
	 */

	$labels = [
		"name" => __( "Karmy Suche", "nroom" ),
		"singular_name" => __( "Karma Sucha", "nroom" ),
	];

	$args = [
		"label" => __( "Karmy Suche", "nroom" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "karmy_suche", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "excerpt", "trackbacks", "custom-fields", "comments", "revisions", "author", "page-attributes", "post-formats" ],
	];

	register_post_type( "karmy_suche", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );

function cptui_register_my_cpts_behawiorysta() {

	/**
	 * Post Type: Behawioryści.
	 */

	$labels = [
		"name" => __( "Behawioryści", "nroom" ),
		"singular_name" => __( "Behawiorysta", "nroom" ),
	];

	$args = [
		"label" => __( "Behawioryści", "nroom" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "behawiorysta", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "excerpt", "custom-fields", "comments" ],
		"taxonomies" => [ "category" ],
	];

	register_post_type( "behawiorysta", $args );
}

add_action( 'init', 'cptui_register_my_cpts_behawiorysta' );


// Wylączenie notice
error_reporting(0);

add_action('plugins_loaded','ao_defer_inline_init');

function ao_defer_inline_init() {
	if ( get_option('autoptimize_js_include_inline') != 'on' ) {
		add_filter('autoptimize_html_after_minify','ao_defer_inline_jquery',10,1);
	}
}

function ao_defer_inline_jquery( $in ) {
  if ( preg_match_all( '#<script.*>(.*)</script>#Usmi', $in, $matches, PREG_SET_ORDER ) ) {
    foreach( $matches as $match ) {
      if ( $match[1] !== '' && ( strpos( $match[1], 'jQuery' ) !== false || strpos( $match[1], '$' ) !== false ) ) {
        // inline js that requires jquery, wrap deferring JS around it to defer it. 
        $new_match = 'var aoDeferInlineJQuery=function(){'.$match[1].'}; if (document.readyState === "loading") {document.addEventListener("DOMContentLoaded", aoDeferInlineJQuery);} else {aoDeferInlineJQuery();}';
        $in = str_replace( $match[1], $new_match, $in );
      } else if ( $match[1] === '' && strpos( $match[0], 'src=' ) !== false && strpos( $match[0], 'defer' ) === false ) {
        // linked non-aggregated JS, defer it.
        $new_match = str_replace( '<script ', '<script defer ', $match[0] );
        $in = str_replace( $match[0], $new_match, $in );
      }
    }
  }
  return $in;
}

add_filter( 'the_content', 'prefix_insert_post_ads' );
function prefix_insert_post_ads( $content ) {
  $ad_code = '<img src="' . get_the_post_thumbnail_url() . '" alt="thumbnail"/>';
  if ( is_single() && ! is_admin() ) {
    return prefix_insert_after_paragraph( $ad_code, 1, $content );
  }
  return $content;
}

function prefix_insert_after_paragraph( $insertion, $paragraph_id, $content ) {
  $closing_p = '</p>';
  $paragraphs = explode( $closing_p, $content );
  foreach ($paragraphs as $index => $paragraph) {
    if ( trim( $paragraph ) ) {
     $paragraphs[$index] .= $closing_p;
    }
    if ( $paragraph_id == $index + 1 ) {
     $paragraphs[$index] .= $insertion;
    }
  }
  return implode( '', $paragraphs );
}

function blazzdev_custom_class( $class ) {
	return $class . ' fas fa-car';
  }
  
  add_filter( 'rmp_rating_icon_class', 'blazzdev_custom_class' );