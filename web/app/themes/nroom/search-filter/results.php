<?php
function excerpt( $limit ) {
$excerpt = explode(' ', get_the_excerpt(), $limit);
if (count($excerpt)>=$limit) {
array_pop($excerpt);
$excerpt = implode(" ",$excerpt).'...';
} else {
$excerpt = implode(" ",$excerpt);
}
$excerpt = preg_replace('`[[^]]*]`','',$excerpt);
return $excerpt;
}
function content($limit) {
$content = explode(' ', get_the_content(), $limit);
if (count($content)>=$limit) {
array_pop($content);
$content = implode(" ",$content).'...';
} else {
$content = implode(" ",$content);
}
$content = preg_replace('/[.+]/','', $content);
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);
return $content;
}
?>
<section class="karmy-results">
            <div class="container">
              <div class="row">
<?php if ( $query->have_posts() ) { ?>

        <?php
        $i = 0;
        $page = $query->query['paged'];
        if ($page > 1) {
          $i = $i + (12 * ($page - 1));
        } else {
          $i = 0;
        }   
        while ($query->have_posts()) { $query->the_post(); $i++; 
          if(isset(get_post_meta( get_the_ID(), "ratings_average")[0])){
            $ratings_average = get_post_meta( get_the_ID(), "ratings_average")[0];
          } else {
            $ratings_average = 0;
          }
          $percent_progres_bar = 2 * $ratings_average * 10;
          $percent_progres_bar = number_format($percent_progres_bar, 2, '.', '');
        ?>   
          
                <div class="col-md-4 col-sm-12">
                  <div class="posts-home__img">
                    <a href="<?php the_permalink(); ?>">
                      <img src="<?php echo get_the_post_thumbnail_url(); ?>"/>
                    </a>
                  </div>
                  <div class="posts-home__content">
                      <a class="title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                      <p><?php echo excerpt(25); ?></p>
                    </div>
                </div>
             
          
         
        <?php } ?>
        
        </div>
            </div>
          </section>
<?php } else { ?>
  <div class="line mt-5"></div>
  <div class="container">
    <div class='search-filter-results-list mt-5' data-search-filter-action='infinite-scroll-end'>
      <span>Koniec wyników</span>
    </div>
  </div>
	
  
<?php } ?>
