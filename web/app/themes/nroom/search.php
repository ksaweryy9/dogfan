<?php
$context                = Timber::get_context();
$search_term            = get_search_query();
$context['title']       = 'Search results for ' . $search_term;
$context['posts']       = new \Timber\PostQuery('ThemePost');
$context['search_term'] = $search_term;

Timber::render( 'views/templates/search.twig', $context );