<?php
$datebase_name = $_POST['db_name'];
$datebase_user = $_POST['db_user'];
$datebase_password = $_POST['db_password'];
$folder_name = current(array_filter(explode("/",parse_url($_SERVER['PHP_SELF'],PHP_URL_PATH))));
$file_conent = "
  DB_NAME=".$datebase_name."
  DB_USER=".$datebase_user."
  DB_PASSWORD=".$datebase_password."

  # Optional variables
  # DB_HOST=localhost
  # DB_PREFIX=wp_

  WP_ENV=development
  WP_HOME=http://".$_SERVER['HTTP_HOST']."/".$folder_name."
  WP_SITEURL=\${WP_HOME}/wp

  # Generate your keys here: https://roots.io/salts.html
  AUTH_KEY='".rand_string(64)."'
  SECURE_AUTH_KEY='".rand_string(64)."'
  LOGGED_IN_KEY='".rand_string(64)."'
  NONCE_KEY='".rand_string(64)."'
  AUTH_SALT='".rand_string(64)."'
  SECURE_AUTH_SALT='".rand_string(64)."'
  LOGGED_IN_SALT='".rand_string(64)."'
  NONCE_SALT='".rand_string(64)."'

  ACF_PRO_KEY=b3JkZXJfaWQ9MzkxNTh8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE0LTA5LTA3IDE5OjUwOjU1
";

file_put_contents('../.env', $file_conent);

if (file_exists('../.env')) {
  echo json_encode(array('success' => 1));
} else {
  echo json_encode(array('success' => 0));
}

function rand_string( $length ) {
  $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_ []{}<>~`+=,.;:/?|";  
  $size = strlen( $chars );
  for( $i = 0; $i < $length; $i++ ) {
    $str .= $chars[ rand( 0, $size - 1 ) ];
  }
  return $str;
}