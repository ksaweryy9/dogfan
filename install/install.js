$(document).ready(function() {
  $('#loginform').submit(function(e) {
    e.preventDefault();
    $('#loginform').addClass('disable');
    $('.loading').addClass('visable');
    $('.comand-line').addClass('visable');
    $('.comand-line pre code').append("Starting 'creating .env file'");
    $.ajax({
      type: "POST",
      url: 'create-env.php',
      data: $(this).serialize(),
      success: function(response) {
        var jsonData = JSON.parse(response);
        if (jsonData.success == "1") {
          setTimeout(function(){ 
            $('.comand-line pre code').append("</br>Finishing 'creating .env file'");
            create_htaccess(); 
          }, 500);
        }
        else {
          $('.comand-line pre code').append("</br><span style='color: red'>Error 'creating .env file'");
        }
      }
    });
  });
});

function create_htaccess(){
  setTimeout(function(){ 
    $('.comand-line pre code').append("</br>Starting 'creating .htaccess'");
    $.ajax({
      type: "POST",
      url: 'create-htaccess.php',
      data: $(this).serialize(),
      success: function(response) {
        var jsonData = JSON.parse(response);
        if (jsonData.success == "1") {
          setTimeout(function(){ 
            $('.comand-line pre code').append("</br>Finishing 'creating .htaccess'");
            create_configyml();
          }, 500);
        }
        else {
          $('.comand-line pre code').append("</br><span style='color: red'>Error 'creating .htaccess'");
        }
      }
    });
  }, 500);
}

function create_configyml() {
  setTimeout(function(){ 
    $('.comand-line pre code').append("</br>Starting 'creating config.yml'");
    $.ajax({
      type: "POST",
      url: 'create-configyml.php',
      data: $(this).serialize(),
      success: function(response) {
        var jsonData = JSON.parse(response);
        if (jsonData.success == "1") {
          setTimeout(function(){ 
            $('.comand-line pre code').append("</br>Finishing 'creating config.yml'");
            composer_install();
          }, 500);
        }
        else {
          $('.comand-line pre code').append("</br><span style='color: red'>Error 'creating .htaccess'");
        }
      }
    });
  }, 500);
}

function composer_install() {
  setTimeout(function(){
    $('.comand-line pre code').append("</br>composer install");
    $('.comand-line pre code').append('</br><span class="loading-dots">Installing</span>');
    $('.comand-line pre').scrollTop($('.comand-line pre')[0].scrollHeight);
    $.ajax({
      type: "POST",
      url: 'composer-install.php',
      success: function(response) {
        var jsonData = JSON.parse(response);
        $('.loading-dots').addClass('disable');
        $('.comand-line pre code').append("</br>"+jsonData.output);
        $('.comand-line pre').scrollTop($('.comand-line pre')[0].scrollHeight);
        npm_install();
      }
    });
  }, 500);
}

function npm_install() {
  setTimeout(function(){
    $('.comand-line pre code').append("</br>npm install");
    $('.comand-line pre code').append('</br><span class="loading-dots">Installing</span>');
    $('.comand-line pre').scrollTop($('.comand-line pre')[0].scrollHeight);
    $.ajax({
      type: "POST",
      url: 'npm-install.php',
      success: function(response) {
        var jsonData = JSON.parse(response);
        $('.loading-dots').addClass('disable');
        $('.comand-line pre code').append("</br>"+jsonData.output);
        $('.comand-line pre').scrollTop($('.comand-line pre')[0].scrollHeight);
        composer_install_2();
      }
    });
  }, 500);
}

function composer_install_2() {
  setTimeout(function(){
    $('.comand-line pre code').append("</br>composer install");
    $('.comand-line pre code').append('</br><span class="loading-dots">Installing</span>');
    $('.comand-line pre').scrollTop($('.comand-line pre')[0].scrollHeight);
    $.ajax({
      type: "POST",
      url: 'composer-install-2.php',
      success: function(response) {
        var jsonData = JSON.parse(response);
        $('.loading-dots').addClass('disable');
        $('.comand-line pre code').append("</br>"+jsonData.output);
        $('.comand-line pre').scrollTop($('.comand-line pre')[0].scrollHeight);
        npm_run_build()
      }
    });
  }, 500);        
}

function npm_run_build() {
  setTimeout(function(){
    $('.comand-line pre code').append("</br>npm run build");
    $('.comand-line pre code').append('</br><span class="loading-dots">Building</span>');
    $('.comand-line pre').scrollTop($('.comand-line pre')[0].scrollHeight);
    $.ajax({
      type: "POST",
      url: 'npm-run-build.php',
      success: function(response) {
        var jsonData = JSON.parse(response);
        $('.loading-dots').addClass('disable');
        $('.comand-line pre code').append("</br>"+jsonData.output);
        $('.comand-line pre').scrollTop($('.comand-line pre')[0].scrollHeight);
        //location.href = window.location.href.replace("/install/", "")+"/web";
      }
    });
  }, 500);
}