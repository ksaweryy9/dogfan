<!doctype html>
<html>
  <head>
  <link rel="stylesheet" href="prism/prism.css" data-noprefix />
  <link rel="stylesheet" href="style.css"/>
  <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
  </head>
  <body>

    <div class="form-container">
      <div class="logo">
        <img src="../web/app/themes/nroom/assets/images/logo.svg" alt="" />
      </div>
      <div class="database-form">
        <form id="loginform" method="post">
          <input type="text" name="db_name" placeholder="datebase name" required>
          <input type="text" name="db_user" placeholder="datebase user" required>
          <input type="text" name="db_password" placeholder="datebase password" required>
          <input type="submit" name="submit" value="Install new nroom">
        </form>
      </div>
      <div class="comand-line">
      <pre><code class="language-bash"></code></pre>
      </div>
    </div>
    

    <script src="prism/prism.js"></script>
    <script src="install.js"></script>
  </body>
</html>