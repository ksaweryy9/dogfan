<?php
$folder_name = current(array_filter(explode("/",parse_url($_SERVER['PHP_SELF'],PHP_URL_PATH))));
$file_conent = "
  <IfModule mod_rewrite.c>
    # Do not change this line.
    RewriteEngine on

    # Change spidersweb.pl to be your primary domain.
    RewriteCond %{HTTP_HOST} ^(www.)?".$_SERVER['HTTP_HOST']."$

    # Change 'web' to be the folder you will use for your primary domain.
    RewriteCond %{REQUEST_URI} !^/".$folder_name."/web/

    #********************
    # NOT RRCOMMENDED   *
    #********************
    #
    # Uncomment those two lines if you want dirs and files to be accessible out of the
    # your primary domain dir.

    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d

    # Change 'web' to be the folder you will use for your primary domain.
    RewriteRule ^(.*)$ /".$folder_name."/web/$1

    # Change spidersweb.pl to be your primary domain again.
    # Change 'web' to be the folder you will use for your primary domain
    # followed by / then the main file for your site, index.php, index.html, etc.
    RewriteCond %{HTTP_HOST} ^(www.)?".$_SERVER['HTTP_HOST']."$
    RewriteRule ^(/)?$ ".$folder_name."/web/index.php [L]
  </IfModule> 
";

file_put_contents('../.htaccess', $file_conent);

if (file_exists('../.htaccess')) {
  echo json_encode(array('success' => 1));
} else {
  echo json_encode(array('success' => 0));
}