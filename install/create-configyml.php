<?php
$folder_name = current(array_filter(explode("/",parse_url($_SERVER['PHP_SELF'],PHP_URL_PATH))));
$file_conent = '
# Your project\'s server will run on localhost:xxxx at this port
DEVURL: "http://'.$_SERVER['HTTP_HOST'].'/'.$folder_name.'"

# Gulp will reference these paths when it copies files
PATHS:
  # Path to dist folder
  dist: "dist"
  # Paths to static assets that aren\'t images, CSS, or JavaScript
  assets:
    - "assets/**/*"
    - "!assets/{img,js,scss}/**/*"
  # Paths to Sass libraries, which can then be loaded with @import
  sass:
    - "node_modules"
  # Paths to JavaScript libraries, which are combined into one file
  # TODO:  Rework this using webpack build. Right now just verifying the files are being built properly
  entries:
    - "assets/js/app.js"
  
  cssfile:
    - "./dist/css/app.css"
';

file_put_contents('../web/app/themes/nroom/config.yml', $file_conent);

if (file_exists('../web/app/themes/nroom/config.yml')) {
  echo json_encode(array('success' => 1));
} else {
  echo json_encode(array('success' => 0));
}
